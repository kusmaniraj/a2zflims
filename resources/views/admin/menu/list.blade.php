@extends('layouts.admin')


@section('content')
<?php


$id = "";
$url = "";
$name = "";
$status = "active";

$state = "Add";
$routeAction = route('menu.store');
$method = "";
if (isset($menu_detail)) {
    $id = $menu_detail['id'];
    $url = $menu_detail['url'];
    $name = $menu_detail['name'];
   
    $status = $menu_detail['status'];
    $state = "Edit";
    $routeAction = route('menu.update', $id);
    $method = method_field('PUT');
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Menu Categories
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                @include('alertMessage')

                <div class="row">
                    <div class="col-sm-6">
                        <button class="btn btn-primary btn-sm" onclick="jstree.jstree('open_all')">Expand All
                        </button>
                        <button class="btn btn-primary btn-sm" onclick="jstree.jstree('close_all')">Close All
                        </button>
                        <button class="btn btn-success btn-sm" id="saveChangesBtn">Save Changes</button>
                        <a href="{{url('control_panel/menu')}}" class="btn btn-danger btn-sm"> <i
                                class="fa fa-refresh"></i></a>
                        <hr>

                        <div class="form-group">
                            <input type="text" id="searchTree" class="form-control" placeholder="Search...">
                        </div>

                        <div id="tree"></div>
                    </div>

                    <div class="col-sm-6">
                        <h4><?= $state ?> Menu Item</h4>
                        <hr>


                        <form action="<?= $routeAction ?>" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            {{$method}}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" name="name" value="<?= $name ?>" class="form-control"
                                       placeholder="Link Name">
                                @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" name="url" class="form-control" placeholder="URL"
                                       value="<?= $url ?>">
                            </div>

                           


                            <div class="form-group">
                                <label for="">Status</label><br>

                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="status"
                                               value="active" <?= ($status == "active") ? 'checked' : ''; ?>>
                                        Active
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="status"
                                               value="inactive" <?= ($status == "inactive") ? 'checked' : ''; ?>>
                                        Inactive
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary"><?= $state ?> menu</button>
                            </div>
                        </form>

                    </div>
                </div>


            </div>
        </div>
       

        </div>

    </section>
</div>



@endsection
@push('scripts')
<link rel="stylesheet" href="{{asset('bower_components/jstree/dist/themes/default/style.min.css')}}"/>
<script src="{{ asset('bower_components/jstree/dist/jstree.min.js')}}"></script>


<script>
    $('.menuForm ').on('click', 'button.getMenuUrl', function () {
        var menuId= $('.menuForm select[name="menuId"]').val();
        if (menuId ==0 ) {
            alert('Please Select Menu Name');
            return false;
        } else {
            $.get("{{url('control_panel/menu/getUrl')}}" + '/' + menuId, function (response) {
                console.log(response);
                var baseUrl = '{{url("/")}}';
                var menuUrlList = '';
                $.each(response, function (i, v) {
                    menuUrlList += '<h3>Parent Url</h3>' +
                        '<ul >' +
                        '<li><strong>' + v.name + ' Url:</strong>' + baseUrl + '/category_type/list/' + v.name + '</li>' +

                        '</ul>' +

                        '<h3>Child Url</h3>';
                    if (v.contents) {
                        $.each(v.contents, function (i, c) {
                            menuUrlList += '<ul >' +
                                '<li><strong>' + c.name + ':</strong>' + baseUrl + '/category_type/detail/' + v.id + '/' + c.id + '</li>' +

                                '</ul>';

                        });
                    }

                });


                $('.menuUrlList').html(menuUrlList);


            })
        }
    })
</script>




<script>
    function customMenu(node) {
        var items = {

            editItem: {
                label: "Edit",
                action: function () {
                    window.location.href = "{{url('control_panel/menu')}}" + "/" + node.id + "/edit";
                }
            },
            deleteItem: {
                label: "Delete",
                action: function () {
                    if (node.children.length) {
                        alert('Can not delete the category which have child categories.');
                        return;
                    }
                    $.ajax({
                        url: "{{url('control_panel/menu/')}}" + "/" + node.id,
                        type: 'DELETE',
                        data: {id: node.id, _token: '{{csrf_token()}}'},
                        success: function () {
                            window.location.href = "{{url('control_panel/menu/')}}";
                        },
                        error: function () {
                            alert('error');

                        }

                    });


//
                }
            }
        };
        return items;
    }

    var jstree = $('#tree');
    $(function () {

        jstree.jstree({
            'plugins': ['contextmenu', 'search', 'dnd'],
            'core': {
                'data': <?php print_r(json_encode($menus)) ?>,
                'themes': {
                    'icons': true
                },
                check_callback: true
            },
            'contextmenu': {
                'items': customMenu
            },
            "search": {
                "case_insensitive": true,
                "show_only_matches": true
            },

        }).on('loaded.jstree', function () {
            jstree.jstree('open_all');
        });
    });


    $("#searchTree").keyup(function () {
        var searchString = $(this).val();
        jstree.jstree('search', searchString);
    });

    var treeExport = [];
    var debunkTree = function (menu, parentID) {

        $.each(menu, function (index, data) {
            if (data.children.length) {
                debunkTree(data.children, data.id);
            }

            treeExport.push({
                id: data.id,
                name: data.text,
                parent_id: parentID,


            });
        });
    };

    $('#saveChangesBtn').on('click', function (e) {
        var menu = jstree.jstree(true).get_json('#', {flat: false});
        treeExport = [];
        debunkTree(menu, 0);
//        console.log(treeExport);

        var params = {treeExport: treeExport, _token: '{{csrf_token()}}',}
        $.ajax({
            url: "{{route('menu.saveMenu')}}",
            type: 'post',
            cache: false,
            data: params,
            success: function (response) {
//                alert(response);
                window.location.href = "{{url('control_panel/menu/')}}";
            },
            error: function () {
                alert('error');
            }
        });
    })


</script>


@endpush