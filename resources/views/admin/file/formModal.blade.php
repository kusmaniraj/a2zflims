<div id="filesFormModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Form</h4>
            </div>
            <div class="modal-body">


                <form id="filesForm" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id">


                    <div class="form-group col-md-12" id="country">
                        <div class="col-md-3">
                            <label for="country">Country <span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <select name="countryId" class="form-control">
                                <option value="0" disabled selected>Select Country</option>
                                @isset($countries)
                                @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->countryName}}</option>
                                @endforeach

                                @endisset


                            </select>
                        </div>


                    </div>


                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="name">File Name <span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name"
                                   value="" required>
                        </div>


                    </div>


                    <div class="form-group col-md-12" id="quality">
                        <div class="col-md-3">
                            <label for="quality">Quality <span class="required">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <select name="quality" class="form-control">
                                <option selected value="0" disabled>Select Quality</option>
                                <option>HD</option>
                                <option>SD</option>
                                <option>TS</option>
                                <option>CAM</option>

                            </select>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="size">
                        <div class="col-md-3">
                            <label for="size">Size</label>
                        </div>
                        <div class="col-md-9">
                            <input type="number" class="form-control" name="size"
                                   value="">
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="length">
                        <div class="col-md-3">
                            <label for="length">Length <span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="number" class="form-control" name="length"
                                   value="" required>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="tmdbId">
                        <div class="col-md-3">
                            <label for="name">TMDb Id </label>
                        </div>
                        <div class="col-md-9">
                            <input type="number" class="form-control" name="tmdbId"
                                   value="">
                        </div>


                    </div>

        
                    <div class="form-group col-md-12" id="meta_title_id">
                        <div class="col-md-3">
                            <label for="name">Meta Title </label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="metaTitle"
                                   value="">
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="meta_description_id">
                        <div class="col-md-3">
                            <label for="name">Meta Description </label>
                        </div>
                        <div class="col-md-9">
                            <textarea rows="3" name="metaDescription" class="form-control"></textarea>
                        </div>


                    </div>


                    <div id="status" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="status">Status</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status" value="active" checked>
                                    Active
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status"
                                           value="inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>


                    </div>
                    <div class=" form-group">
                        <button class="btn btn-primary pull-right " type="submit"></button>
                        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>


                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>