<div id="serversFormModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Form</h4>
            </div>
            <div class="modal-body">


                <form id="serversForm" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id">

                    <div class="form-group col-md-12" id="name">
                        <div class="col-md-3">
                            <label for="name">Server Name <span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <select name="name" class="form-control">
                                <option value="0" disabled selected>Select Server</option>
                                <option value="openload_user">Openload User</option>
                                <option value="openload_original">Openload link</option>
                                <option value="youtube">Youtube</option>
                                <option value="playercdn"> Player cdn(Rapid Video)</option>
                            </select>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="link">
                        <div class="col-md-3">
                            <label for="link">Server File Link <span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="link"
                                   value="" required>
                        </div>


                    </div>





                    <div class=" form-group">
                        <button class="btn btn-primary pull-right " type="submit"></button>
                        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>


                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>