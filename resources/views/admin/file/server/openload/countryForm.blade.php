

<div id="countryFormModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Country Form</h4>
            </div>
            <div class="modal-body">


                <form id="countryForm" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id">
                     <input type="hidden" name="countryId">

                    
                   
                    <div class="form-group col-md-12" id="name">
                        <div class="col-md-3">
                            <label for="name">Country Name <span class="required">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="countryName"
                                   value="" required>
                        </div>


                    </div>
                   
                    <div id="status" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="status">Status</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status" value="active" checked>
                                    Active
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status"
                                           value="inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>


                    </div>
                    <div class=" form-group">
                        <button class="btn btn-primary pull-right " type="submit">Update</button>
                        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>