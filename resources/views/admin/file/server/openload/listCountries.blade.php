@extends('layouts.admin')


@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Openload Countries Management
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                @include('alertMessage')

              <div class="row">
              
                    <div class="col-md-12">
                        <a href="#" id="addCountriesBtn" class=" pull-right btn btn-primary btn-flat">Import/Refresh Countries</a>
                    </div>

                    <div class="col-md-12">
                        <table id="countriesTable" class="table table-bordered">

                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Country Name</th>
                                <th>Status</th>
                                <th>Action</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                         </table>
                    </div>
                    
               
                 
              </div>


            </div>
        </div>
       

        </div>

    </section>
</div>

<!-- country update form -->
@include('admin.file.server.openload.countryForm')

@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('build/admin/js/countries.js')}}"></script>
@endpush