@extends('layouts.admin')


@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Openload Files Management
        </h1>
    </section>
   

    <section class="content">
          <div class="box" id="selectCountryPage">
            <div class="box-body">
                <div class="col-md-6"> 
                        <div class="col-md-10 form-group">
                                <select name="country" class="form-control">
                                        <option value="0" disabled selected>  Select Country</option> 
                                        @isset($countries)
                                                @foreach($countries as $country)
                                                   <option value="{{$country->id}}" data-countryId="{{$country->countryId}}">  {{ucfirst($country->countryName)}}</option> 
                                                @endforeach
                                        @endisset   
                                </select>   
                        </div>
                        <div class="col-md-2">
                             <a href="#" id="selectCountryBtn" class=" pull-right btn btn-primary btn-flat">Select</a>
                        </div>
                </div>
             



            </div>
        </div>
        <div class="box" id="filePage">
            <div class="box-body">
                @include('alertMessage')

              <div class="row">
              
                    <div class="col-md-12">
                       <div class="col-md-10"> 
                       <p class="text-info">  <strong>   Info : </strong>Listed Files will be selected country!.</p>   
                       </div>
                       <div class="col-md-2">  
                         <a href="#" id="importFilesBtn" class=" pull-right btn btn-default btn-flat"><i class="fa fa-file-movie-o">   </i>&nbsp;Import /Refresh Files</a>  
                       </div>
                      
                    </div>

                    <div class="col-md-12">
                        <table id="filesTable" class="table table-bordered">

                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Imdb Id</th>

                                <th>Status</th>
                                <th>Action</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                         </table>
                    </div>
                    
               
                 
              </div>


            </div>
        </div>
       

        </div>

    </section>
</div>

<!-- openload foem country update form -->
@include('admin.file.server.openload.fileFormModal')

@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('build/admin/js/openloadFiles.js')}}"></script>
@endpush