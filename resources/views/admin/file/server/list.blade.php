<div class="content-wrapper " id="serverPage" style="display: none">
    <section class="content-header">
        <h1>
            Server Management
        </h1>
    </section>


    <section class="content">

        <div class="box" >
            <div class="box-body">
                @include('alertMessage')

                <div class="row">

                    <div class="col-md-12">
                        <div class="col-md-10">
                            <a href="#"  class=" showFilePageBtn btn btn-default btn-flat"><i
                                    class="fa fa-file-movie-o"> </i>&nbsp;Back To File Page</a>
                        </div>
                        <div class="col-md-2">

                            <a href="#" id="addServersBtn" class=" pull-right btn btn-default btn-flat"><i
                                    class="fa fa-file-movie-o"> </i>&nbsp;Add Server</a>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <table id="serversTable" class="table table-bordered">

                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


                </div>


            </div>
        </div>



    </section>
</div>

<!-- server form -->
@include('admin.file.server.formModal')