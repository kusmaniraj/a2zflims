@extends('layouts.admin')


@section('content')

<div class="content-wrapper" id="filePage">
    <section class="content-header">
        <h1>
            Files Management
        </h1>
    </section>


    <section class="content">

        <div class="box" >
            <div class="box-body">
                @include('alertMessage')

                <div class="row">

                    <div class="col-md-12">
                        <div class="col-md-10">

                        </div>
                        <div class="col-md-2">
                            <a href="#" id="addFilesBtn" class=" pull-right btn btn-default btn-flat"><i
                                    class="fa fa-file-movie-o"> </i>&nbsp;Add Files</a>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <table id="filesTable" class="table table-bordered">

                            <thead>
                            <tr>
                                <th>SN</th>

                                <th>Country</th>
                                <th>Name</th>
                                <th>Imdb Id</th>
                                <th>Server</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


                </div>


            </div>
        </div>



    </section>
</div>

<!--server page-->



<!-- file  form -->
@include('admin.file.formModal')


@endsection
@push('scripts')

<!--jquery Ui-->
<script src="{{asset('packages/jquery-ui/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('build/admin/js/files.js')}}"></script>

@endpush
