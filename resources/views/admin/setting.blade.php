@extends('layouts.admin')



@section('content')
<?php


if(!empty($settings)){

    $id = $settings['id'];
    $website_name = $settings['website_name'];
    $website_name_short = $settings['website_name_short'];
    $website_notice=$settings['website_notice'];
    $fb_url = $settings['fb_url'];
    $fb_page = $settings['fb_page'];
    $logo = $settings['logo'];
    $google_plus_url = $settings['google_plus_url'];
    $twitter_url = $settings['twitter_url'];
    $skype_url = $settings['skype_url'];
    $slogan = $settings['slogan'];
    $short_description = $settings['short_description'];
    $google_maps_iframe = $settings['google_maps_iframe'];
    $owner_name = $settings['owner_name'];
    $owner_email = $settings['owner_email'];
    $owner_contact = $settings['owner_contact'];
    $owner_address = $settings['owner_address'];
    $version = $settings['version'];
    $routeAction=route('setting.update',$id);
    $method=method_field('PUT');




}else{
    $id="";
    $website_name="";
    $website_name_short="";
    $website_notice="";
    $fb_url="";
    $twitter_url="";
    $skype_url="";
    $google_plus_url="";
    $fb_page="";
    $logo="";
    $slogan="";
    $short_description="";
    $google_maps_iframe="";
    $owner_email="";
    $owner_address="";
    $owner_name="";
    $owner_contact="";
    $version="";
    $routeAction=route('setting.store');
    $method="";
}

?>
<div class="content-wrapper">
    <div>
        @include('alertMessage')
    </div>

    <section class="content-header">
        <h1>
            Settings
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Website Information</h3>
            </div>
            <div class="box-body">


                <p class="text-muted"><strong>Note:</strong> The data of settings will be used in the website. Please
                    provide correct information.</p>

                <form action="{{$routeAction}}" method="post" enctype="multipart/form-data">
                    {{$method}}
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="website_name">Website Name</label>
                                <input type="text" class="form-control" id="website_name " name="website_name"
                                       value="{{ $website_name }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="website_name_short">Website Short Name</label>
                                <input type="text" class="form-control" id="website_name_short"
                                       name="website_name_short"
                                       value="{{$website_name_short }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="$website_notice">Website Notice</label>
                                <textarea class="form-control" id="$website_notice"
                                       name="website_notice" rows="5">
                                    {{ $website_notice }}
                                    </textarea>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-4">
                             <div class="form-group row">
                                    <input   class="form-control hidden " type="text" value="{{$logo}}" name="oldLogo">
                                    <input type="file" name="logo" class="hidden">
                                    <div class="col-md-6">
                                          <button type="button" class="btn btn-primary" id="logoImgBtn">Select Logo Image</button>
                                    </div>
                                    <div  class="col-md-4">
                                        <img src="{{asset('files/1/logo/'.$logo) }}" id="imgPreview" height="200px" width="200px" class="img-thumbnail hidden">
                                    </div>
                                  
                                    

                                </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="fb_url">Facebook Page/Profile Url</label>
                                <input type="text" class="form-control" id="fb_url" name="fb_url"
                                       value="{{ $fb_url }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="google_plus_url">Google Plus Url</label>
                                <input type="text" class="form-control" id="google_plus_url" name="google_plus_url"
                                       value="{{ $google_plus_url}}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="skype_url">Skype Url</label>
                                <input type="text" class="form-control" id="skype_url" name="skype_url"
                                       value="{{ $skype_url}}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="twitter_url">Twitter Url</label>
                                <input type="text" class="form-control" id="twitter_url" name="twitter_url"
                                       value="{{ $twitter_url}}">
                            </div>
                        </div>


                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="google_maps_iframe">Google Maps Iframe</label>
                                <textarea name="google_maps_iframe" id="google_maps_iframe" rows="5"
                                          class="form-control"><?= $google_maps_iframe ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fb_page">Facebook page Iframe</label>
                                <textarea name="fb_page" id="fb_page" rows="5"
                                          class="form-control">{{ $fb_page }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="slogan">Website Slogan/Tag Line</label>
                                <textarea name="slogan" id="slogan" rows="5"
                                          class="form-control"><?= $slogan ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea name="short_description" id="short_description" rows="5"
                                          class="form-control"><?= $short_description ?></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="owner_name">Company/Owner Name</label>
                                <input type="text" class="form-control" id="owner_name" name="owner_name"
                                       value="<?= $owner_name ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="owner_email">Company/Owner Email</label>
                                <input type="email" class="form-control" id="owner_email" name="owner_email"
                                       value="<?= $owner_email ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="owner_address">Company/Owner Address</label>
                                <input type="text" class="form-control" id="owner_address" name="owner_address"
                                       value="<?= $owner_address ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="owner_contact">Company/Owner Contact No.</label>
                                <input type="text" class="form-control" id="owner_contact" name="owner_contact"
                                       value="<?= $owner_contact ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="version">System Version</label>
                                <input
                                    type="text" class="form-control" id="version" name="version"
                                    value="<?= $version ?>">
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="<?= $id ?>">

                    <div class="form-group">
                        <button class="btn btn-primary mr15" type="submit">Save Changes</button>
                        <button class="btn btn-danger" type="button" id="cancelBtn"
                                onclick="window.history.back()">Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>




@endsection
@push('scripts')

<script>
$('#logoImgBtn').on('click',function(){
    $('input[name="logo"]').click();
})

$('input[name="logo"]').on('change',function() {
 


  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imgPreview').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
  }
   $('#imgPreview').removeClass('hidden');

});


    var img = "{{$logo}}";
    if (img != "") {
        $('#imgPreview').removeClass('hidden');
    }
</script>
@endpush