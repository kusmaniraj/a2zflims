<!DOCTYPE html>
<html lang="en">
<meta name="google-site-verification" content="4G86cKObi3pgehQ5c4zWTGwLQviMsUHtbNEYBlbb5wo" />
@stack('metas')
@include('web.includes.head')

<body>
<!--loader-->
<div class="loading"></div>
<!--End of Loader-->


<!--header-->
@include('web.includes.header')
@stack('styles')
<!--End of Headet-->

<!-- nav -->
@include('web.includes.navbar')
<!-- //nav -->

<!--social icons-->
<!--<div class="general_social_icons">-->
<!--    <nav class="social">-->
<!--        <ul>-->
<!--            <li class="w3_twitter"><a href="#">Twitter <i class="fa fa-twitter"></i></a></li>-->
<!--            <li class="w3_facebook"><a href="#">Facebook <i class="fa fa-facebook"></i></a></li>-->
<!--            <li class="w3_dribbble"><a href="#">Dribbble <i class="fa fa-dribbble"></i></a></li>-->
<!--            <li class="w3_g_plus"><a href="#">Google+ <i class="fa fa-google-plus"></i></a></li>-->
<!--        </ul>-->
<!--    </nav>-->
<!--</div>-->
<!--End of Social Icon-->

<!--Main Content-->
@yield('content')
<!--End of main content-->

<!-- footer -->
@include('web.includes.footer')

<!-- //footer -->

<!--script-->
@include('web.includes.script')
@stack('scripts')
<!--End of Script-->


</body>


<!--Medianet-->
<script type="text/javascript">
    window._mNHandle = window._mNHandle || {};
    window._mNHandle.queue = window._mNHandle.queue || [];
    medianet_versionId = "3121199";
</script>


</html>
