@extends('layouts.web')
@section('content')

<!--list Movies-->
<div class="general-agileits-w3l">
    <div class="w3l-medile-movies-grids">

        <!-- /movie-browse-agile -->

        <div class="movie-browse-agile">
            <!--/browse-agile-w3ls -->
            <div class="browse-agile-w3ls general-w3ls">
                <div class="tittle-head ">
                    <div class="col-md-12">
                        <div class="row">
                            <h4 class="latest-text">Results search: @isset($results_name) <span style="text-transform: none"> {{ucFirst($results_name)}}</span>@endisset</h4>
                        </div>
                    </div>


                    <!---728x90--->


                </div>
                <!---728x90--->

                <div class="container">
                    <div class="browse-inner">

                        @isset($lists)

                        @forelse ($lists as $video)
                        <div class="col-md-2  w3l-movie-gride-agile">
                            <a href="{{$video->media_type=='movie' ? route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))]) : route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}"
                               class="hvr-shutter-out-horizontal"><img class="poster-img" src="{{$video->poster_path}}"
                                                                       title="{{$video->original_title}}"
                                                                       alt=" {{$video->title}}">

                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>

                            <div class="mid-1">
                                <div class="w3l-movie-text">
                                    <h6>
                                        <a href="{{$video->media_type=='movie' ? route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))]) : route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a>
                                    </h6>
                                </div>
                                <div class="mid-2">

                                    <p> {{$video->release_year}}</p>

                                    <div class="block-stars">
                                        {!!$video->rating_stars!!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            @if($video->release_type ==='New')
                            <div class="ribben">
                                <p>NEW</p>
                            </div>
                            @endif
                        </div>


                        @empty
                        <p>No results found....</p>
                        @endforelse
                        @endisset

                        <div class="clearfix"></div>
                    </div>


                </div>
            </div>
            <!--//browse-agile-w3ls -->
            <div class="blog-pagenat-wthree">
                @isset($lists)
                @if($lists)
                {{$lists->appends(Request::except('page'))->links()}}
                @endif
                @endisset

            </div>
        </div>
        <!-- //movie-browse-agile -->


    </div>
    <!-- //w3l-medile-movies-grids -->
</div>
<!--end of list movies-->
@endsection

@push('scripts')
<script>

</script>
@endpush
