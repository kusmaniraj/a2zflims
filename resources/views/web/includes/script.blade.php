<!-- Bootstrap Core JavaScript -->
<script src="{{asset('web/js/bootstrap.min.js')}}"></script>

<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('web/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->


<!-- //Bootstrap Core JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({easingType: 'easeOutQuart'});

        // hide loader
        $(window).on('load', function () {
            $('.loading').hide();
        })
        setTimeout(() => {
            $('.loading').hide();
        }, 10000)//hide after 10 sec


    });
</script>

<!--Equal Height-->
<script>
    var maxHeight = 260;
    var maxWidth = 185;
    var $selector = $('.poster-img');
    $selector.each(function () {
        var poster_height = $(this).height();
        var poster_width = $(this).width();
//        console.log(poster_height)
        if (poster_height > maxHeight) {
            maxHeight = poster_height;
        }
        if (poster_width > maxWidth) {
            maxWidth = poster_width;
        }

    });
    $selector.height(maxHeight);
    $selector.width(maxWidth)
</script>
<!--end of equal height-->
<!-- //here ends scrolling icon -->

<!-- pop-up-box -->
<script src="{{asset('web/js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
<!--//pop-up-box -->
<script>
    $(document).ready(function () {
        $('.w3_play_icon,.w3_play_icon1,.w3_play_icon2').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });

    });
</script>



