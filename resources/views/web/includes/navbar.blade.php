


<!-- //bootstrap-pop-up -->
<div class="movies_nav">
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<nav>
					<ul class="nav navbar-nav">
						<li class="{{@$title=='home' ? 'active':''}}"><a href="/">Home</a></li>
						<li class="dropdown {{@$title=='genres' ? 'active':''}}" >
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Genres <b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<li>
									@isset($genres)
									@foreach(array_chunk($genres,5) as $chunkArray)

									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											@foreach($chunkArray as $name=>$genre)

											<li><a href="{{route('web.listGenresVideos',$genre['name'])}}">{{ucfirst($genre['name'])}}</a></li>
											@endforeach


										</ul>
									</div>
									@endforeach
									@endisset

									<div class="clearfix"></div>
								</li>
							</ul>
						</li>
						<li class="{{@$title=='movies' ? 'active':''}}"><a href="{{route('web.listMovies')}}">Movies</a></li>
						<li class="{{@$title=='tv-series' ? 'active':''}}"><a href="{{route('web.listSeries')}}" >tv - series</a></li>

						<li class="dropdown {{@$title=='countries' ? 'active':''}}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Country <b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3" style="width: 560px">
								<li>
									@isset($countries)
									@foreach(array_chunk($countries,5) as $chunkArray)

									<div class="col-sm-3">

										<ul class="multi-column-dropdown">

											@foreach($chunkArray as $name=>$country)
											<li><a href="{{route('web.listCountryVideos',str_replace(' ','-',$country['name']))}}">{{$country['name']}}</a></li>

											@endforeach

										</ul>

									</div>
									@endforeach
									@endisset

								</li>
							</ul>
						</li>

					</ul>
				</nav>
			</div>
		</nav>
	</div>
</div>
