<div class="header">
   <div class="container">
      <div class="w3layouts_logo">
         <a href="/"><h1>A2Z<span>Vids</span></h1></a>
      </div>
      <div class="w3_search">
         <form action="{{route('web.search')}}" method="get">
            <input type="text" name="v" placeholder="Search for a movie,tv-series " required="" value="{{request()->has('v') ? request()->get('v'):'' }}">
            <input type="submit" value="Go" class="search_btn" >
         </form>
      </div>

      <div class="clearfix"></div>
   </div>
</div>
@push('script')
<script>
   $('.search_btn').on('click',function(){
      var search_field=$('input[name="v"]').val();
      if(search_field==""){
         return false;
      }
   })
</script>
@endpush
