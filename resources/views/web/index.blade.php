@extends('layouts.web')
@push('styles')
<style>
    p.slidey-overlay-title a{
        color: #fff !important;
    }
</style>
@endpush
@section('content')

<!-- banner -->
<div id="slidey" style="display:none;">
    <ul>
        @isset($now_playing)
        @foreach($now_playing as $video)
        <li><img src="{{$video->backdrop_path}}" alt="{{$video->title}} ">

            <p class='title'> <a  href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->original_title}}</a></p>

            <p class='description'> {{$video->overview}}.</p></li>
        @endforeach
        @endisset


    </ul>
</div>




<!-- Movies -->
<div class="general">
    <h4 class="latest-text w3_latest_text">Featured Movies</h4>
    <!---728x90--->

    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#popular" id="popular-tab" role="tab" data-toggle="tab"
                                                          aria-controls="home" aria-expanded="true">Popular</a></li>

                <li role="presentation"><a href="#rating" id="rating-tab" role="tab" data-toggle="tab"
                                           aria-controls="rating" aria-expanded="true">Top Rating</a></li>


            </ul>
            <div id="myTabContent" class="tab-content">

                <div role="tabpanel" class="tab-pane fade active in" id="popular" aria-labelledby="popular-tab">
                    @isset($popular)
                    @foreach($popular as $video)
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}" class="hvr-shutter-out-horizontal"><img
                                src="{{$video->poster_path}}" title="{{$video->title}}" class=" poster-img img-responsive "
                                alt="{{$video->title}} "/>

                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>

                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p>{{$video->release_year}}</p>

                                <div class="block-stars">
                                    {!!$video->rating_stars!!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if($video->release_type ==='New')
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endisset
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="rating" aria-labelledby="rating-tab">
                    @isset($top_rated)
                    @foreach($top_rated as $video)
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}" class="hvr-shutter-out-horizontal"><img
                                src="{{$video->poster_path}}" title="{{$video->title}}" class="poster-img img-responsive"
                                alt="{{$video->title}} "/>

                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>

                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p>{{$video->release_year}}</p>

                                <div class="block-stars">
                                    {!!$video->rating_stars!!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if($video->release_type ==='New')
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endisset
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- //movies -->

<!--Medianet add-->
<div id="821650362">
    <script type="text/javascript">
        try {
            window._mNHandle.queue.push(function (){
                window._mNDetails.loadTag("821650362", "728x90", "821650362");
            });
        }
        catch (error) {}
    </script>
</div>
<!--/Medianet add-->
<!-- Latest-tv-series -->
<div class="general">
    <h4 class="latest-text w3_latest_text">Featured TV Series</h4>
    <!---728x90--->

    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#popular_tv" id="popular_tv-tab" role="tab" data-toggle="tab"
                                                          aria-controls="home" aria-expanded="true">Popular</a></li>

                <li role="presentation"><a href="#rating_tv" id="rating_tv-tab" role="tab" data-toggle="tab"
                                           aria-controls="rating" aria-expanded="true">Top Rating</a></li>

            </ul>
            <div id="myTabContent" class="tab-content">

                <div role="tabpanel" class="tab-pane fade active in" id="popular_tv" aria-labelledby="popular_tv-tab">
                    @isset($popularTvSeries)
                    @foreach($popularTvSeries as $video)
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="{{route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}" class="hvr-shutter-out-horizontal"><img
                                src="{{$video->poster_path}}" title="{{$video->title}}" class="img-responsive"
                                alt="{{$video->title}} "/>

                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>

                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="{{route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p>{{$video->release_year}}</p>

                                <div class="block-stars">
                                    {!!$video->rating_stars!!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if($video->release_type ==='New')
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endisset
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="rating_tv" aria-labelledby="rating_tv-tab">
                    @isset($top_ratedTvSeries)
                    @foreach($top_ratedTvSeries as $video)
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="{{route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}" class="hvr-shutter-out-horizontal"><img
                                src="{{$video->poster_path}}" title="{{$video->title}}" class="img-responsive"
                                alt="{{$video->title}} "/>

                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>

                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="{{route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p>{{$video->release_year}}</p>

                                <div class="block-stars">
                                    {!!$video->rating_stars!!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if($video->release_type ==='New')
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    @endisset
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End of Latest Tv Series-->
@endsection

@push('scripts')

<!--slidey-->
<script src="{{asset('web/js/jquery.slidey.js')}}"></script>
<script src="{{asset('web/js/jquery.dotdotdot.min.js')}}"></script>
<script type="text/javascript">

    $("#slidey").slidey({
        interval: 8000,
        listCount: 5,
        autoplay: false,
        showList: true
    });
    $(".slidey-list-description").dotdotdot();
</script>
<!--end slidey-->






@endpush
