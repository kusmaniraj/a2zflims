@extends('layouts.web')
@push('metas')
<meta name="keywords" content="{{$details->info->title}}">
<meta name="description" content="{{$details->info->overview}}">
@endpush
@push('styles')

@endpush
@section('content')
<div class="single-page-agile-main">
    <div class="container">
        <!-- /w3l-medile-movies-grids -->
        <div class="agileits-single-top">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url($details->info->media_type==='movie' ? 'movies':'series')}}">{{ucfirst($details->info->media_type)}}</a>
                    </li>
                    <li class="active">{{$details->info->original_title}}</li>
                </ol>
            </div>

        </div>
        <div class="single-page-agile-info">

            <!-- /movie-browse-agile -->
            <div class="show-top-grids-w3lagile">
                <div class="col-sm-12 single-left">

                    @isset($showVideos)
                    <!--Stream and Download-->
                    <div class="stream-schedule " style="margin: 5px 0px">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="#" target="_blank" class="btn  btn-info">Stream in HD&nbsp;<i class="fa fa-play "></i></a>&nbsp;
                                <a href="https://9xbuddy.com/process?url={{$details->defaultVideo}}" target="_blank"
                                   class="btn  btn-info">Download in HD&nbsp;<i class="fa fa-download"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--/Stream and Download-->
                    <!--embed video-->
                    <div class="song ">

                        <div class="video-grid-single-page-agileits">
                            <div data-video="dLmKio67pVQ" id="video">
                                <iframe id="ytplayer" type="text/html" src="{{$showVideos[0]}}"
                                        frameborder="0" allowFullScreen="true" webkitallowfullscreen="true"
                                        mozallowfullscreen="true"></iframe>

                            </div>
                        </div>
                    </div>
                    <!--/embed video-->



                    <!--Server List-->
                    @isset($showVideos)
                    <div class="servers">

                        <ul>
                            @php $i=1;@endphp
                            @foreach($showVideos as $path)
                            <li><a href="#" class="btn btn-server {{($i==1 ? 'active':'')}}" data-src="{{$path}}"
                                   onclick="fnAddServerToIFrame(this)"><i
                                            class="fa fa-server"></i>&nbsp;Server {{$i}}</a></li>
                            @php $i++; @endphp
                            @endforeach


                        </ul>

                    </div>
                    @endisset
                    <!--End of Server List-->
                    @endisset
                    <!--video info-->
                    <div class="video-info">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{$details->info->poster_path}}" alt="{{$details->info->title}}"
                                     class="img-responsive">
                            </div>
                            <div class="col-md-9">
                                <div class="title">
                                    <h3>{{$details->info->title}}</h3>
                                    <span class="trailer" style="margin-top: -15px">
                                        <a href="#" data-src="{{$details->trailerVideo}}" class=" trailer-btn "
                                           onclick="fnWatchTrailerToIFrame(this)"><i
                                                    class="fa fa-video-camera"></i>&nbsp;Watch Trailer</a></span>


                                </div><!--title-->
                                <div class="rated-star">
                                    {!!$details->info->rating_stars!!}
                                </div><!--rating star-->
                                <div class="description">
                                    <p>{{$details->info->overview}}</p>
                                </div><!--description-->
                                <!-- Additional Info-->
                                <div class="mvic-info">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!---genres-->
                                            @isset($details->info->genres)
                                            <p>
                                                <strong>Genre:&nbsp;</strong>
                                                @foreach($details->info->genres as $genre)
                                                <a href="{{route('web.listGenresVideos',$genre->name)}}">{{$genre->name}}</a>,&nbsp;
                                                @endforeach
                                            </p>
                                            @endisset
                                            <!---/genres-->
                                            <!---credits -->
                                            @isset($details->info->credits)
                                            <p>
                                                <strong>Director:&nbsp;</strong>

                                                @foreach($details->info->credits->crew as $crew)
                                                @if($crew->department=='Directing')
                                                <a href="{{route('web.listMediaByType',[$details->info->media_type,'crew',str_replace(' ','-',strtolower($crew->name))])}}">{{$crew->name}}</a>
                                                @endif
                                                @endforeach

                                            </p><!--crews-->
                                            <p>
                                                <strong>Actors:&nbsp;</strong>

                                                @foreach(array_slice($details->info->credits->cast,0,10) as $cast)
                                                <a href="{{route('web.listMediaByType',[$details->info->media_type,'cast',str_replace(' ','-',strtolower($cast->name))])}}">{{$cast->name}}</a>,
                                                @endforeach

                                            </p><!--casts-->
                                            @endisset
                                            <!---/credits-->

                                            <!---/companies-->
                                            @isset($details->info->production_companies)
                                            <p>
                                                <strong>Studios:&nbsp;</strong>

                                                @foreach($details->info->production_companies as $company)
                                                <a href="{{route('web.listMediaByType',[$details->info->media_type,'company',str_replace(' ','-',strtolower($company->name))])}}">{{$company->name}}</a>,&nbsp;
                                                @endforeach

                                            </p>
                                            @endisset
                                            <!---/companies-->


                                            <!---/networks-->
                                            @isset($details->info->networks)
                                            <p>
                                                <strong>Networks:&nbsp;</strong>

                                                @foreach($details->info->networks as $network)
                                                <a href="{{route('web.listMediaByType',[$details->info->media_type,'network',str_replace(' ','-',strtolower($network->name))])}}">{{$network->name}}</a>,&nbsp;
                                                @endforeach

                                            </p>
                                            @endisset
                                            <!---/networks-->
                                            <!---countries-->
                                            @isset($details->info->production_countries)
                                            <p>
                                                <strong>Countries:&nbsp;</strong>

                                                @foreach($details->info->production_countries as $country)
                                                <a href="{{route('web.listCountryVideos',$country->name)}}">{{$country->name}}</a>,&nbsp;
                                                @endforeach

                                            </p>
                                            @endisset
                                            <!---/countries-->



                                        </div>
                                        <div class="col-md-6">
                                            <!---duration-->
                                            <p><strong>Duration:&nbsp;</strong>
                                                {{$details->info->runtime}} min</p>
                                            <!---/duration-->

                                            <!---quality-->
                                            <p><strong>Quality:&nbsp;</strong>
                                                <label class="label label-success quality">SD</label></p>
                                            <!---/quality-->

                                            <!---Release-->
                                            <p><strong>Release:&nbsp;</strong>
                                                {{$details->info->release_year}}</p>
                                            <!---/Release-->

                                            <!---/vote_average-->
                                            <p><strong>IMDB:&nbsp;</strong>
                                                <label
                                                        class="label label-success imdb">{{$details->info->vote_average}}</label>
                                            </p>
                                            <!---/vote_average-->
                                        </div>

                                    </div>


                                </div>
                                <!-- /Additional Info-->

                            </div>


                        </div>
                    </div>
                    <!--end of video info-->



                    <div class="clearfix"></div>
                </div>
                <!-- //movie-browse-agile -->


            </div>

            <div class="session-episodes">
              <div class="row">
                  <div class="col-sm-12">
                      <h3>Session and Episodes</h3>
                      <div class="panel-group">
                          @isset($details->info->seasons)
                          @foreach($details->info->seasons as $key=>$seasons)
                          @if($seasons->season_number > 0)

                          <div class="panel panel-info">
                              <div class="panel-heading">
                                  <h4 class="panel-title">
                                      <a data-toggle="collapse" href="#collapse{{$key}}" class="img-responsive"><img height="60px"  src="{{config('tmdb.profile_path').$seasons->poster_path}}" alt="{{$seasons->name}}"> &nbsp; {{$seasons->name}}</a>
                                  </h4>
                              </div>
                              <div id="collapse{{$key}}" class="panel-collapse collapse {{$seasons->season_number==1 ? 'in':''}}" >
                                  @for($i=1;$i <= $seasons->episode_count;$i++)

                                  <ul class="list-group">
                                      <li class="list-group-item"><a href="{{route('web.detailTv',str_replace(' ','-',strtolower($details->info->title))).'?season='.$seasons->season_number.'&episode='.$i}}">{{$seasons->season_number}} - Episode {{$i}} </a></li>
                                  </ul>
                                  @endfor

                              </div>
                          </div>
                          @endif
                          @endforeach
                          @endisset
                      </div>
                  </div>
              </div>
            </div> <!-- session-episodes-->
            <!--        similat movies-->
            <div class="browse-inner">
                @isset($similarVideos)
                @foreach($similarVideos as $video)
                <div class="col-md-2  w3l-movie-gride-agile">
                    <a href="{{$video->media_type=='movie' ? route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))]) : route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}"
                       class="hvr-shutter-out-horizontal"><img
                                class="poster-img" src="{{$video->poster_path}}" title="{{$video->original_title}}"
                                alt=" {{$video->title}}">

                        <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                    </a>

                    <div class="mid-1">
                        <div class="w3l-movie-text">
                            <h6>
                                <a href="{{$video->media_type=='movie' ? route('web.detailMovie',[str_replace(' ','-',strtolower($video->title))]) : route('web.detailTv',[str_replace(' ','-',strtolower($video->title))])}}">{{$video->title}}</a>
                            </h6>
                        </div>
                        <div class="mid-2">

                            <p>{{$video->release_year}}</p>

                            <div class="block-stars">
                                {!!$video->rating_stars!!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    @if($video->release_type ==='New')
                    <div class="ribben">
                        <p>NEW</p>
                    </div>
                    @endif

                </div>
                @endforeach
                @endisset


                <div class="clearfix"></div>
            </div>
            <!--        end of similar movies-->

            <div id="comments">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                    var disqus_config = function () {
                    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    */
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://a2zvids.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


            </div>


        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $loaderSel = $('.loading')

    function fnAddServerToIFrame(self) {
        $loaderSel.show();
        const src = $(self).attr('data-src');
        $('.servers .btn-server').removeClass('active');
        $(self).addClass('active');
        $('iframe').attr('src', src);
        setTimeout(() => {
            $loaderSel.hide();
        }, 1000)
    }

    function fnWatchTrailerToIFrame(self) {
        $loaderSel.show();
        const src = $(self).attr('data-src');
        $('iframe').attr('src', src);
        setTimeout(() => {
            $loaderSel.hide();
        }, 1000)
    }

    if ($('#c_window_xEucqIjg').length > 0) {
        $('#c_window_xEucqIjg').remove();
    }

</script>
@endpush



