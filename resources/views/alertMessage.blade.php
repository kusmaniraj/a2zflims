@if(Session::get('success'))
                        <div id="msg-alert" class="alert alert-success alert-dismissable">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong>Successfully!</strong> {{Session::get('success')}}.
</div>

@endif

@if(Session::get('error'))
<div id="msg-alert" class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> {{Session::get('error')}}.
</div>

@endif
<div style="display: none" id="error-msg-alert" class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong>
</div>
<div style="display: none" id="success-msg-alert" class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Successfully !</strong>
</div>



<div id="alertMessage" class="alert " style="display: none;">
</div>

<!--validation error-->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif