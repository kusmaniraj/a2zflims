$('<div id="loading"/>').css({

    width: $(window).width() + 'px',
    height: $(window).height() + 'px',

}).hide().appendTo('body');

$(document)
    .ajaxStart(function(){
      $("#loading").fadeIn(500);

    })
    .ajaxStop(function(){
      $("#loading").fadeOut(500);
    });