var list_page_module=new ListPage();
function ListPage() {
	
	$self=this;
	var $page=$('#list_video_page');
	var list_type=$page.data('type');
    var video_type=$page.data('video_type');
	var genre_id=$page.data('genre_id');
    var cast_id=$page.data('cast_id');
    var credit_id=$page.data('credit_id');
	var original_language=$page.data('original_language');
	var query=$page.data('query');
	var page_size=12;
	var page_number=1;


	this.bind_function=function(){

		
           $('ul.pagination').on('click','li a',function(){
          
           	var pageNum=$(this).text();
            var previousPageNum=$('ul.pagination li a.active').text();
            $self.pagination(pageNum,previousPageNum);
             

          
          		
           	
           		// remove all css of active
				
           })
	}
    this.pagination=function(pageNum,previousPageNum){
        
         $('ul.pagination li a').css({'background':'','color':''}).removeClass('active');
                if(pageNum=='«'){
                    if(previousPageNum > 0){
                         page_size=(12*(parseInt(previousPageNum)-1));
                          $self.getVideosList(page_size,1);
                     }else{
                         $self.getVideosList(12,1);
                     }

                 

                }else if(pageNum=='»'){

                    if(previousPageNum < 5){
                      
                        
                        page_size=(12*(parseInt(previousPageNum)+1));
                          
                          $self.getVideosList(page_size,1);
                     }else{
                         $self.getVideosList(60,1);
                     }
                   
                }else{
                    page_size=(12*pageNum);
                     $self.getVideosList(page_size,1);
                }

                 $('ul.pagination li a').each(function(){
                    var value=$(this).text();
                    // console.log(value);
                 
                    if(value==page_size/12){
                      
                        $(this).css({'background':'#23527c','color':'#fff'}).addClass('active');
                    }
                 })
    }


	this.getVideosList=function(pageSize,pageNumber){

		
		page_size=pageSize;		
		page_number=pageNumber;
		
		if(list_type=='country'){
			var optParam={language:'en-US','with_original_language':original_language,'page':1,'sort_by':'popularity.desc'};
			$self.getCountryVideosList(pageSize,pageNumber,optParam);
		}else if(list_type=='genre'){
			var optParam={language:'en-US','genre_id':genre_id,'page':1,'sort_by':'popularity.desc'};

			$self.getGenreVideosList(pageSize,pageNumber,optParam);
		}else if(list_type=='search'){
			var optParam={language:'en-US','page':1};
			$self.getSearchVideosList(query,pageSize,pageNumber,optParam);
		}else if(list_type=='movies'){
			var optParam={language:'en-US','page':1};
			$self.getAllVideosList(pageSize,pageNumber,optParam);
		
		
        }else if(list_type=='tv_series'){
            var optParam={language:'en-US','page':1};
            $self.getAllVideosList(pageSize,pageNumber,optParam);
        
        }else if(list_type=='actor'){
            var optParam={language:'en-US','page':1,'cast_id':cast_id};
            $self.getCastVideosList(pageSize,pageNumber,optParam);
			
		}else if(list_type=='writer' || list_type=='director'){
            var optParam={language:'en-US','page':1,'credit_id':cast_id};
            $self.getCrewVideosList(pageSize,pageNumber,optParam);
        }else{
            return false;
        }



		
	}
	
	this.getAllVideosList=function(pageSize,pageNumber,optParam){
		tmdb_module.getAllVideosList(video_type,optParam,function(data){
           
             // console.log(data.results)
             $self.arraySlice(data.results,function(data){
             	  $self.printListVideos(data);
             	
             })
              
        })
	}
	this.getCountryVideosList=function(pageSize,pageNumber,optParam){
		tmdb_module.getCountryVideosList(video_type,optParam,function(data){
           
             // console.log(data.results)
             $self.arraySlice(data.results,function(data){
             	  $self.printListVideos(data);
             	
             })
              
        })
	}
	this.getGenreVideosList=function(pageSize,pageNumber,optParam){
      
		tmdb_module.getGenreVideosList(video_type,optParam,function(data){
           
            // console.log(data.results)
             $self.arraySlice(data.results,function(data){
             	  $self.printListVideos(data);
             	 
             })
              
        })
	}
	this.getSearchVideosList=function(query,pageSize,pageNumber,optParam){
		tmdb_module.getSearchVideosList(video_type,query,optParam,function(data){
           
            // console.log(data.results)
             $self.arraySlice(data.results,function(data){
             	  $self.printListVideos(data);
             	 
             })
              
        })
	}
	
    this.getCastVideosList=function(pageSize,pageNumber,optParam){
        tmdb_module.getCastVideosList(video_type,optParam,function(data){
           
             // console.log(data.results)
             $self.arraySlice(data.results,function(data){
                  $self.printListVideos(data);
                
             })
              
        })
    }
     this.getCrewVideosList=function(pageSize,pageNumber,optParam){
        tmdb_module.getCrewVideosList(video_type,optParam,function(data){
           
             // console.log(data.results)
             $self.arraySlice(data.results,function(data){
                  $self.printListVideos(data);
                 
             })
              
        })
    }
	this.printListVideos=function(data){
     // console.log(data);
			list_movies_html='';
			var img_url='http://image.tmdb.org/t/p/w185/';
			$.each(data,function(i,v){

					list_movies_html +='<article class="col-lg-2 col-md-4 col-sm-4">'+
                             
                              '<div class="post post-medium">'+
                                 '<div class="thumbr">';
                                 if(v.title){
                                  list_movies_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/movie/'+v.id+'/'+v.title.replace(/\s/g,'_')+'" >';
                                }else{
                                  list_movies_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/tv/'+v.id+'/'+v.name.replace(/\s/g,'_')+'" >';
                                }
                                   
                                  list_movies_html +='<span class="play-btn-border" title="Play"><i class="fa fa-play-circle headline-round" aria-hidden="true"></i></span>'+
                                       // '<div class="cactus-note ct-time font-size-1"><span>02:02</span></div>'+
                                       '<img class="img-responsive" src="'+img_url+v.poster_path+'" alt="#">'+
                                    '</a>'+
                                 '</div>'+
                                 '<div class="infor">';
                                    if(v.title){
                                      list_movies_html +='<h4>'+
                                                       '<a class="title" href="#">'+v.title+'</a>'+
                                                    '</h4>';
                                    }else{
                                       list_movies_html +='<h4>'+
                                                       '<a class="title" href="#">'+v.name+'</a>'+
                                                    '</h4>';
                                    }
                                  
                                    list_movies_html += '<span class="posts-txt" title="Posts from Channel"><i class="fa fa-thumbs-up" aria-hidden="true"></i>'+v.popularity+'</span> &nbsp;'+
                                     '<span class="posts-txt" title="Posts from Channel"><span class="tmdb-btn">IMDb</span> &nbsp; '+v.vote_average+'</span>'+
                                 '</div>'+
                              '</div>'+
                           '</article>';
                         
                           
			});
			$page.html(list_movies_html);

	}
	
    this.arraySlice=function(array,callback){

          --page_number; // because pages logically start with 1, but technically with 0
             var data= array.slice(page_number * page_size, (page_number + 1) * page_size);
             // console.log(data);
             callback(data);
              
    }
	
	
	this.init=function(){
			$self.bind_function();
			$self.getVideosList(12,1);
			
	}();



}