var home_page_module=new HomePage();
function HomePage() {
	
	$self=this;
var genreArray=[];

	this.bind_function=function(){

           $self.getGenreList();
	}

  // get Genre
  // genre list
  this.getGenreList=function(){
    tmdb_module.getGenreList({language:'en-US'},function(data){
                   // console.log(data.genres);
     
      $.each(data.genres,function(i,v){
         genreArray[i]={'name':v.name,'id':v.id}
      })
    
      })

  }
  // Movies --------------------------------------------Page

  // popular movies
	this.getPopularMovies=function(){
		tmdb_module.getVideos('movie','popular',{language:'en-US',page:1},function(data){
                   // console.log(data);
                $self.printVideos('#latest_movie_tab_page',data.results);
        })
	}
	
  // rated movies
  this.getRatedMovies=function(){
    tmdb_module.getVideos('movie','top_rated',{language:'en-US',page:1},function(data){
                  // console.log(data);
                $self.printVideos('#rated_movie_tab_page',data.results);
        })
  }
 

  // latest movies
  this.getLatestMovies=function(){
    tmdb_module.getVideos('movie','now_playing',{language:'en-US',page:1},function(data){
                 // console.log(data);
                $self.printVideos('#most_view_movie_tab_page',data.results);
        })
  }

  // latest movies
  this.getUpCommingMovies=function(){
    tmdb_module.getVideos('movie','upcoming',{language:'en-US',page:1},function(data){
                 // console.log(data);
                  $self.printUpcommingMoviesInSlider('#upcoming_movies_slider_page',data.results)
                $self.printVideos('#upcoming_movies_page',data.results);
               
        })
  }

 

  //Tv serries --------------------------------- Page


   // popular Tv series
  this.getPopularTvSeries=function(){
    tmdb_module.getVideos('tv','popular',{language:'en-US',page:1},function(data){
                   // console.log(data);
                $self.printVideos('#latest_tv_series_tab_page',data.results);
        })
  }
  
  // rated Tv series
  this.getRatedTvSeries=function(){
    tmdb_module.getVideos('tv','top_rated',{language:'en-US',page:1},function(data){
                  // console.log(data);
                $self.printVideos('#rated_tv_series_tab_page',data.results);
        })
  }
 

  // latest Tv series
  this.getLatestTvSeries=function(){
    tmdb_module.getVideos('tv','airing_today',{language:'en-US',page:1},function(data){
                 // console.log(data);
                $self.printVideos('#most_view_tv_series_tab_page',data.results);
        })
  }
   this.getUpCommingTvSeries=function(){
    tmdb_module.getVideos('tv','on_the_air',{language:'en-US',page:1},function(data){
                 // console.log(data);
                $self.printVideos('#upcoming_tv_series_page',data.results);
        })
  }


  // Print------------------------Videos
  this.printVideos=function(selector,data){
    
      movie_html='';
      var img_url='http://image.tmdb.org/t/p/w185';
      $.each(data,function(i,v){

          movie_html +=' <article class="col-lg-2 col-md-4 col-sm-4">'+
                             
                              '<div class="post post-medium">'+
                                 '<div class="thumbr">';
                                 if(v.title){
                                  movie_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/movie/'+v.id+'/'+v.title.replace(/\s/g,'_')+'" >';
                                }else{
                                  movie_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/tv/'+v.id+'/'+v.name.replace(/\s/g,'_')+'" >';
                                }
                                   
                                  movie_html +='<span class="play-btn-border" title="Play"><i class="fa fa-play-circle headline-round" aria-hidden="true"></i></span>'+
                                       // '<div class="cactus-note ct-time font-size-1"><span>02:02</span></div>'+
                                       '<img class="img-responsive" src="'+img_url+v.poster_path+'" alt="#">'+
                                    '</a>'+
                                 '</div>'+
                                 '<div class="infor">';
                                    if(v.title){
                                      movie_html +='<h4>'+
                                                       '<a class="title" href="'+base_url+'/viewDetail/movie/'+v.id+'/'+v.title.replace(/\s/g,'_')+'">'+v.title+'</a>'+
                                                    '</h4>';
                                    }else{
                                       movie_html +='<h4>'+
                                                       '<a class="title" href="'+base_url+'/viewDetail/tv/'+v.id+'/'+v.name.replace(/\s/g,'_')+'">'+v.name+'</a>'+
                                                    '</h4>';
                                    }
                                  
                                    movie_html += '<span class="posts-txt" title="Posts from Channel"><i class="fa fa-thumbs-up" aria-hidden="true"></i>'+v.popularity+'</span> &nbsp;'+
                                     '<span class="posts-txt" title="Posts from Channel"><span class="tmdb-btn">IMDb</span> &nbsp; '+v.vote_average+'</span>'+
                                 '</div>'+
                              '</div>'+
                           '</article>';
                           if(i==11){
                            return false;
                           }
      });
      $(selector).html(movie_html);

  }
 
  // *******************Up Comming Movies************************
  this.printUpcommingMoviesInSlider=function(selector,data){

    
     movie_html='';
   
     
      var img_url='http://image.tmdb.org/t/p/w780';
      $.each(data,function(i,v){
        var genreName=[];
        $.each(v.genre_ids,function(genreIdIndex,genreIdValue){
          $.each(genreArray,function(genreIndex,genreValue){
             if(genreIdValue==genreValue.id){
                genreName.push(genreValue.name);
             }
          })
           

        })
       
        

          movie_html +='<li >'+
                            '<img src="'+img_url+v.backdrop_path+'" class="img-responsive" />'+
                            '<div class="col-md-12 flex-caption"> '+
                              '<ul type="none"> '; 
                               if(v.title){
                                       movie_html +='<a href="'+base_url+'/viewDetail/movie/'+v.id+'/'+v.title.replace(/\s/g,'_')+'"><h2>'+v.title+'</h3></a>';
                                    }else{
                                       movie_html +='<a href="'+base_url+'/viewDetail/tv/'+v.id+'/'+v.name.replace(/\s/g,'_')+'"><h2> '+v.name+'</h3></a>';
                                    } 
                              
                              movie_html+= '<li> '+
                               '<span class="tmdb_rating"><span class="tmdb-btn" aria-hidden="true">IMDb</span>&nbsp;'+v.vote_average+'</span>&nbsp;&nbsp; &nbsp;     <span class="thumbs_up "><i class="fa fa-thumbs-up" aria-hidden="true"></i> &nbsp;'+v.popularity+'</span></li>'+
                               '<li>Genre: '+genreName.join(',')+'</li>'+
                               '</ul>'+
                              '<p class="flex-video-info">'+ v.overview+
                             
                              '</p>'+
                           
                          
                            '</div>'+
    
     

                      '</li>';
                           if(i==9){
                            return false;
                           }
      });
        $(selector).html(movie_html);

  }

	
	this.init=function(){
			$self.bind_function();
      // movies
			$self.getPopularMovies();
      $self.getRatedMovies();
      $self.getLatestMovies();
      $self.getUpCommingMovies();
      // tv series
        $self.getPopularTvSeries();
      $self.getRatedTvSeries();
      $self.getLatestTvSeries();
      $self.getUpCommingTvSeries();
	}();



}