var tmdb_module=new Tmdb();
function Tmdb() {
	$self=this;
	var api_key='334356bcbc84f0fb74dc4ff94a01728e';
	var tmdb_movie_url='https://api.themoviedb.org/3/';
	var tmdb_genre_movie_url='https://api.themoviedb.org/3/genre/movie/list';
	var tmdb_discover_url='https://api.themoviedb.org/3/discover/';
	
	var tmdb_search_url='https://api.themoviedb.org/3/search/';
	this.bind_function=function(){

	}


	// required type like rated,popular,upcomming,optParam in object type {language:'en-US','page':1},callback function
	this.getVideos=function(type,videoType,optParam,callback){
		var url=tmdb_movie_url+type+'/'+videoType+'?api_key='+api_key;
		if(optParam.language){
			url +='&language='+optParam.language;
		}
		if(optParam.page){
			url +='&page='+optParam.page;
		}
		if(optParam.region){
			url +='&region='+optParam.region;
		}
		$.get(url,function(response){
					callback(response);
		})
	}
//required movie id,optParam in object type {language:'en-US','page':1},callback function
	this.getDetailVideo=function(type,typeId,optParam,callback){
			var url=tmdb_movie_url+type+'/'+typeId+'?api_key='+api_key;
		if(optParam.language){
			url +='&language='+optParam.language;
		}
		if(optParam.append_to_response){
			url +='&append_to_response='+optParam.append_to_response;
		}
		$.get(url,function(response){
					callback(response);
		})
	}
	// required movie id,optParam in object type {language:'en-US','page':1},callback function
	this.getMovieVideos=function(movieId,optParam,callback){
		var url=tmdb_movie_url+movieId+'/videos?api_key='+api_key;
		if(optParam.language){
			url +='&language='+optParam.language;
		}
			$.get(url,function(response){
					callback(response);
		})
	}

	// required movie id,optParam in object type {language:'en-US','page':1},callback function
	this.getSimilarVideos=function(type,typeId,optParam,callback){
		var url=tmdb_movie_url+type+'/'+typeId+'/similar?api_key='+api_key;
		if(optParam.language){
			url +='&language='+optParam.language;
		}
			$.get(url,function(response){
					callback(response);
		})
	}

// optParam in object type {language:'en-US','page':1},callback function
	this.getGenreList=function(optParam,callback){
		var url=tmdb_genre_movie_url+'?api_key='+api_key;
		if(optParam.language){
			url +='&language='+optParam.language;
		}
			$.get(url,function(response){
					callback(response);
		})
	}
	this.getAllVideosList=function(type,optParam,callback){
			var url=tmdb_discover_url+type+'?api_key='+api_key;
		
			
			if(optParam.year){
					url +='&year='+optParam.year;
			}
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.sort_by){
				url +='&sort_by='+optParam.sort_by;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}
	this.getGenreVideosList=function(type,optParam,callback){
		var url=tmdb_discover_url+type+'?api_key='+api_key;
		
			if(optParam.genre_id){
					url +='&with_genres='+optParam.genre_id;
			}
			if(optParam.year){
					url +='&year='+optParam.year;
			}
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.sort_by){
				url +='&sort_by='+optParam.sort_by;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}

	this.getCountryVideosList=function(type,optParam,callback){
		var url=tmdb_discover_url+type+'?api_key='+api_key;
		
			if(optParam.with_original_language){
					url +='&with_original_language='+optParam.with_original_language;
			}
			if(optParam.year){
					url +='&year='+optParam.year;
			}
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.sort_by){
				url +='&sort_by='+optParam.sort_by;
			}
			if(optParam.region){
				url +='&region='+optParam.region;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}

	this.getCastVideosList=function(type,optParam,callback){
		var url=tmdb_discover_url+type+'?api_key='+api_key;
		
			if(optParam.cast_id){
					url +='&with_cast='+optParam.cast_id;
			}
			if(optParam.year){
					url +='&year='+optParam.year;
			}
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.sort_by){
				url +='&sort_by='+optParam.sort_by;
			}
			if(optParam.region){
				url +='&region='+optParam.region;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}
	this.getCrewMoviesList=function(type,optParam,callback){
		var url=tmdb_discover_url+type+'?api_key='+api_key;
		
			if(optParam.credit_id){
					url +='&with_crew='+optParam.credit_id;
			}
			if(optParam.year){
					url +='&year='+optParam.year;
			}
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.sort_by){
				url +='&sort_by='+optParam.sort_by;
			}
			if(optParam.region){
				url +='&region='+optParam.region;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}


	this.getSearchVideosList=function(type,query,optParam,callback){
		var url=tmdb_search_url+type+'?query='+query+'&api_key='+api_key;
		
			
			
			if(optParam.language){
					url +='&language='+optParam.language;
			}
			if(optParam.page){
				url +='&page='+optParam.page;
			}
			

		
		
			$.get(url,function(response){
					callback(response);
			})
	}
	

	this.init=function(){
			$self.bind_function();
	}();



}