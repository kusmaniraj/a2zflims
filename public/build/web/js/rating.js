
function Rating() {
$self=this;

  // genre list
this.ratedStar=function(ratingAverage,callback){
  var html="";
  

  if(ratingAverage>=10){
    html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>';

  }else if(ratingAverage < 10 && ratingAverage >= 9){
     html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-half-o" aria-hidden="true"></i>';

  }else if(ratingAverage < 9 && ratingAverage >= 8){
      
 html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';

  }else if(ratingAverage < 8 && ratingAverage >= 7){
html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-half-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';
  }else if(ratingAverage < 7 && ratingAverage >= 6){
    html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';
 
  }else if(ratingAverage < 6 && ratingAverage >= 5){
    html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-half-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';

  }else if(ratingAverage < 5 && ratingAverage >= 4){
   
 html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';
  }else if(ratingAverage < 4 && ratingAverage >= 3){
    html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-half-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';

  }else if(ratingAverage < 3 && ratingAverage >= 2){
    html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';

  }else if(ratingAverage < 2 && ratingAverage >= 1){
     html +='<i class="fa fa-star-half-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';
   

  }else {
     html +='<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>'+
          '<i class="fa fa-star-o" aria-hidden="true"></i>';

  }
  callback(html);



 

}
this.init=function(){

}();
}