var detail_page_module=new DetailPage();
function DetailPage() {
	
	$self=this;
	var $page=$('#detail_video_page');
	var id=$page.data('id');
  var type=$page.data('type');



	this.bind_function=function(){
		
           
	}
  
	this.getVideoDetail=function(){
		var img_url='http://image.tmdb.org/t/p/w185/';
		tmdb_module.getDetailVideo(type,id,{language:'en-US','append_to_response':'videos,credits'},function(data){


    
     
      // actors
        var actorHtml='Actors : &nbsp;';
               $.each(data.credits.cast,function(i,v){
                actorHtml +='<a href="'+base_url+'/listVideo/'+type+'/actor?cast_id='+v.id+'&name='+v.name.replace(/\s/g,'_')+'"><span class="active">'+v.name+'</span></a>,';
                if(i==4){
                   return false;
                }
               
                })
        $page.find('#video-description li.actors').html(actorHtml);

           // writer
         var writerHtml='Writers : &nbsp;';
           var directorHtml='Directors : &nbsp;';
               $.each(data.credits.crew,function(i,v){
                if(v.department=='Writing'){
                  
                   writerHtml +='<a href="'+base_url+'/listVideo/'+type+'/writer?credit_id='+v.id+'&name='+v.name.replace(/\s/g,'_')+'"><span class="active">'+v.name+'</span></a>,';
                }
                if(v.department=='Directing'){
                  
                   directorHtml +='<a href="'+base_url+'/listVideo/'+type+'/director?credit_id='+v.id+'&name='+v.name.replace(/\s/g,'_')+'"><span class="active">'+v.name+'</span></a>,';
                }
              
               
               
                })
        $page.find('#video-description li.writer').html(writerHtml);
         $page.find('#video-description li.director').html(directorHtml);
   
              $page.find('.poster-img').html('<img class="img-responsive" src="'+img_url+data.poster_path+'" alt="'+data.title+'">')
              $page.find('.main-head-title').text(data.original_title);
              $page.find('.thumbs_up').html('<i class="fa fa-thumbs-up" aria-hidden="true"></i> &nbsp;'+data.popularity+'');
                $page.find('.tmdb_rating').html('<span class="tmdb-btn" aria-hidden="true">IMDb</span>&nbsp;'+data.vote_average+'');
              $page.find('.video-overview p').text(data.overview);
// genres
              var genreHtml='Genres : &nbsp;';
              $.each(data.genres,function(i,v){
              		genreHtml +='<a href="'+base_url+'/listVideo/'+type+'/genre?genre_id='+v.id+'" data-id="'+v.id+'">'+v.name+'</a>';
              });
               $page.find('#video-description li.genres').html(genreHtml);

               // spoken language
              var langHtml='Spoken Languages : &nbsp;';
              $.each(data.spoken_languages,function(i,v){
              		langHtml +='<a href="#">'+v.name+'</a>';
              });
               $page.find('#video-description li.spoken_languages').html(langHtml);

  // production countries
              var contryHtml='Countries: &nbsp;';
              $.each(data.production_countries,function(i,v){
              			contryHtml +='<a href="'+base_url+'/listVideo/'+type+'/country?name='+v.name.replace(/\s/g,'_')+'&original_language='+data.original_language+'" data-id="'+v.id+'">'+v.name+'</a>';
              });
               $page.find('#video-description li.countries').html(contryHtml);

               // released
                $page.find('#video-description li.release_date').html('Released : &nbsp;<a href="#">'+ data.release_date+'</a>');
                  // rating
                $page.find('#video-description li.rating').html('Rating : &nbsp;<a href="#">'+ data.vote_average+'</a>');
           
// iframe
            if($page.find('.video-info iframe,.video-info video').length ==0){

              youtube_url='https://www.youtube.com/embed/';
              let iframeHtml='<iframe src="'+youtube_url+data.videos.results[0].key+'"   class="embed-responsive-item" allowfullscreen></iframe>';
              $page.find('.video-info .video-embed-box').html(iframeHtml);
             }
// rating
   var rating=new Rating();
   rating.ratedStar(data.vote_average,function(resp){
     $page.find('.rating_stars').html(resp);
   })

        })
	}
	this.getMovieVideos=function(){
		tmdb_module.getMovieVideos(id,{language:'en-US'},function(data){
           
             if($page.find('.video-info iframe').length ==0){

             	youtube_url='https://www.youtube.com/embed/';
             	let iframeHtml='<iframe src="'+youtube_url+data.results[0].key+'"   class="embed-responsive-item" allowfullscreen></iframe>';
             	$page.find('.video-info .video-embed-box').html(iframeHtml);
             }
              
        })
	}

	this.getSimilarVideos=function(){
		tmdb_module.getSimilarVideos(type,id,{language:'en-US'},function(data){
         // console.log(data);
      related_video_html='';
      var img_url='http://image.tmdb.org/t/p/w185/';
      $.each(data.results,function(i,v){

          related_video_html +='<article class="col-lg-2 col-md-4 col-sm-4">'+
                             
                              '<div class="post post-medium">'+
                                 '<div class="thumbr">';
                                 if(v.title){
                                  related_video_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/movie/'+v.id+'/'+v.title.replace(/\s/g,'_')+'" >';
                                }else{
                                  related_video_html += '<a class="afterglow post-thumb" href="'+base_url+'/viewDetail/tv/'+v.id+'/'+v.name.replace(/\s/g,'_')+'" >';
                                }
                                   
                                  related_video_html +='<span class="play-btn-border" title="Play"><i class="fa fa-play-circle headline-round" aria-hidden="true"></i></span>'+
                                       // '<div class="cactus-note ct-time font-size-1"><span>02:02</span></div>'+
                                       '<img class="img-responsive" src="'+img_url+v.poster_path+'" alt="#">'+
                                    '</a>'+
                                 '</div>'+
                                 '<div class="infor">';
                                    if(v.title){
                                      related_video_html +='<h4>'+
                                                       '<a class="title" href="#">'+v.title+'</a>'+
                                                    '</h4>';
                                    }else{
                                       related_video_html +='<h4>'+
                                                       '<a class="title" href="#">'+v.name+'</a>'+
                                                    '</h4>';
                                    }
                                  
                                    related_video_html += '<span class="posts-txt" title="Posts from Channel"><i class="fa fa-thumbs-up" aria-hidden="true"></i>'+v.popularity+'</span> &nbsp;'+
                                     '<span class="posts-txt" title="Posts from Channel"><span class="tmdb-btn">IMDb</span> &nbsp; '+v.vote_average+'</span>'+
                                 '</div>'+
                              '</div>'+
                           '</article>';
                         
                           if(i==11){
                            return false;
                           }
      });
      $('#related_videos_page').html(related_video_html);
              
        })
	}
	
	
	
	this.init=function(){
			$self.bind_function();
      $self.getSimilarVideos();
			$self.getVideoDetail();
			// $self.getMovieVideos();
			
	}();



}