var server_module = new Servers();
function Servers() {
    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    var $serverTableName = $('#serversTable');
    var $serverFormName = $('#serversForm');
    var $serverFormModal = $('#serversFormModal');
    var serverDataTable="";

    var serverId;
    var fileId;

    this.bindFunction = function () {

        $('#addServersBtn').on('click', function () {
            $self.emptyForm();
            $self.hideError();
            $serverFormName.find('button[type="submit"]').text('Add');
            $serverFormModal.find('.modal-title').text('Add Form');
            $serverFormModal.modal('show');
        })
        // edit
        $serverTableName.on('click', '.editServerBtn', function () {
            $serverFormName.find('button[type="submit"]').text('Update');
            $serverFormModal.find('.modal-title').text('Update Form');
            var id = $(this).data('id');

            $self.editServer(id);
            $serverFormModal.modal('show');
        })
        // update
        $serverFormName.on('submit', function (e) {
            e.preventDefault();

            $self.hideError();
            var id = $serverFormName.find('input[name="id"]').val();
            if (id) {
                $self.updateServer(id);
            } else {
                $self.storeServers(id);
            }
        })
        // delete
        $serverTableName.on('click', '.delServerBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are you sure want to remove') == true) {

                $self.deleteServer(id);
                return true;
            } else {
                return false;
            }


        })
        //show server Page
        $('#serverPage').on('click', '.showFilePageBtn', function () {
            serverId = $(this).data('id');

            $self.showFilePage('serverPage', 'filePage');
        })



    }
    this.initializeTable = function () {
        serverDataTable = $serverTableName.DataTable({
            processing: true,
        });
    }

    this.getStoreServers = function (fileID) {
        fileId=fileID;
        $.get(base_url + '/control_panel/fileMgmt/server/getServers/'+fileID, function (response) {
            var serverHtml = '';
            $.each(response, function (i, v) {
                serverHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +

                    '<td>' + v.name + '</td>' +

                    '<td>' + v.link + '</td>' +

                    '<td>'+
                    '<a class="btn btn-info editServerBtn " href="#" data-id="' + v.id + '"><i class="  fa fa-edit"></i></a> &nbsp;' +
                    '<a class="remove btn btn-danger delServerBtn " href="#" data-id="' + v.id + '"><i class="  fa fa-remove"></i></a>' +
                    '</td>' +
                    '</tr>';
            })
            if(serverDataTable !=""){
                serverDataTable.destroy();
            }

            $serverTableName.find('tbody').html(serverHtml);
            $self.initializeTable();
        })

    }

    this.storeServers = function () {
        let formData = $serverFormName.serialize() + '&_token=' + csrfToken +'&fileId='+fileId;
        $.post(base_url + '/control_panel/fileMgmt/server/store', formData, function (response) {
            console.log(response);
            if (response.status == 'validation_errors') {
                $self.formValidations(response.msg);
            } else if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreServers(fileId);
                console.log('success store server list');
                $serverFormModal.modal('hide');
            }

        })


    }
    this.editServer = function (id) {

        $.get(base_url + '/control_panel/fileMgmt/server/edit/' + id, function (response) {
            $serverFormName.find('input[name="id"]').val(response.id);
            $serverFormName.find('select[name="name"]').val(response.name);

            $serverFormName.find('input[name="link"]').val(response.link);



        })
    }

    this.updateServer = function (id) {
        let formData = $serverFormName.serialize() + '&_token=' + csrfToken;
        $.post(base_url + '/control_panel/fileMgmt/server/update/' + id, formData, function (response) {
            $self.getStoreServers(fileId);

            if (response.status == 'validation_errors') {
                $self.formValidations(response.msg);
            } else if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreServers(fileId);
                console.log('success update server list');
                $serverFormModal.modal('hide');
            }
        })
    }
    // delete
    this.deleteServer = function (id) {

        $.get(base_url + '/control_panel/fileMgmt/server/delete/' + id, function (response) {

            if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreServers(fileId);
                console.log('success update server');

            }

        })
    }
    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $serverFormName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }


    this.showFilePage = function (nextSelector, prevSelector) {

        $('#'+nextSelector).hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#'+prevSelector).show('slide', {direction: 'left'}, 500);
        }, 300);
    }

    this.hideError = function () {
        $serverFormName.find('.form_errors').hide();


    }

    this.emptyForm = function () {
        $serverFormName[0].reset();
        $serverFormName.find('textarea').val('');
        $serverFormName.find('input[name="id"]').val('');




    }
    this.init = function () {

        $self.bindFunction();




    }();
}