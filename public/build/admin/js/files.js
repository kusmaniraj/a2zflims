var file_module = new Files();
function Files() {
    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    var $fileTableName = $('#filesTable');
    var $fileFormName = $('#filesForm');
    var $fileFormModal = $('#filesFormModal');
    var fileDataTable = "";
    var fileId;


    this.bindFunction = function () {

        $('#addFilesBtn').on('click', function () {
            $self.emptyForm();
            $self.hideError();
            $fileFormName.find('button[type="submit"]').text('Add');
            $fileFormModal.find('.modal-title').text('Add Form');
            $fileFormModal.modal('show');
        })
        // edit
        $fileTableName.on('click', '.editFileBtn', function () {
            $fileFormName.find('button[type="submit"]').text('Update');
            $fileFormModal.find('.modal-title').text('Update Form');
            var id = $(this).data('id');

            $self.editFile(id);
            $fileFormModal.modal('show');
        })
        // update
        $fileFormName.on('submit', function (e) {
            e.preventDefault();

            $self.hideError();
            var id = $fileFormName.find('input[name="id"]').val();
            if (id) {
                $self.updateFile(id);
            } else {
                $self.storeFiles(id);
            }
        })
        // delete
        $fileTableName.on('click', '.delFileBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are you sure want to remove') == true) {

                $self.deleteFile(id);
                return true;
            } else {
                return false;
            }


        })
        //show server Page
        $fileTableName.on('click', '.showServersPage', function () {
            fileId = $(this).data('id');


            $self.showServerPage('filePage', 'serverPage');
        })


    }
    this.initializeTable = function () {
        fileDataTable = $fileTableName.DataTable({
            processing: true,
        });
    }

    this.getStoreFiles = function () {
        $.get(base_url + '/control_panel/fileMgmt/getFiles', function (response) {
            var fileHtml = '';
            $.each(response, function (i, v) {
                fileHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +

                    '<td>' + v.country.countryName + '</td>' +
                    '<td>' + v.name + '</td>' +
                    '<td>' + v.tmdbId + '</td>' +
                    '<td><a href="#" data-id="' + v.id + '" class="showServersPage">Servers <label class="badge badge">'+v.servers.length+'</label></a> </td>' +
                    '<td>' + v.status + '</td>' +
                    '<td>' +

                    '<a class="btn btn-info editFileBtn " href="#" data-id="' + v.id + '"><i class="  fa fa-edit"></i></a> &nbsp;' +
                    '<a class="remove btn btn-danger delFileBtn " href="#" data-id="' + v.id + '"><i class="  fa fa-remove"></i></a>' +
                    '</td>' +
                    '</tr>';
            })
            if (fileDataTable != "") {
                fileDataTable.destroy();
            }

            $fileTableName.find('tbody').html(fileHtml);
            $self.initializeTable();
        })

    }

    this.storeFiles = function () {
        let formData = $fileFormName.serialize() + '&_token=' + csrfToken;
        $.post(base_url + '/control_panel/fileMgmt/store', formData, function (response) {
            console.log(response);
            if (response.status == 'validation_errors') {
                $self.formValidations(response.msg);
            } else if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreFiles();
                console.log('success store file list');
                $fileFormModal.modal('hide');
            }

        })


    }
    this.editFile = function (id) {

        $.get(base_url + '/control_panel/fileMgmt/edit/' + id, function (response) {
            $fileFormName.find('input[name="id"]').val(response.id);
            $fileFormName.find('input[name="name"]').val(response.name);

            $fileFormName.find('input[name="tmdbId"]').val(response.tmdbId);
            $fileFormName.find('input[name="metaTitle"]').val(response.metaTitle);
            $fileFormName.find('textarea[name="metaDescription"]').val(response.metaDescription);
            $fileFormName.find('select[name="countryId"]').val(response.countryId);
            $fileFormName.find('input[name="status"]').prop(response.status);
            $fileFormName.find('input[name="length"]').val(response.length);
            $fileFormName.find('select[name="quality"]').val(response.quality);

        })
    }

    this.updateFile = function (id) {
        let formData = $fileFormName.serialize() + '&_token=' + csrfToken;
        $.post(base_url + '/control_panel/fileMgmt/update/' + id, formData, function (response) {
            $self.getStoreFiles();

            if (response.status == 'validation_errors') {
                $self.formValidations(response.msg);
            } else if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreFiles();
                console.log('success update file list');
                $fileFormModal.modal('hide');
            }
        })
    }
    // delete
    this.deleteFile = function (id) {

        $.get(base_url + '/control_panel/fileMgmt/delete/' + id, function (response) {

            if (response.status == 'error') {
                new alertMessage('error', response.msg).printMessage();
            } else {
                new alertMessage('success', response.msg).printMessage();
                $self.getStoreFiles();
                console.log('success update file');

            }

        })
    }
    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $fileFormName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }
    //*************Server Method*********

    this.showServerPage = function (nextSelector, prevSelector) {

        $('#' + nextSelector).hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#' + prevSelector).show('slide', {direction: 'left'}, 500);
            //get Server of selected  file I
            server_module.getStoreServers(fileId);
        }, 300);
    }

    this.hideError = function () {
        $fileFormName.find('.form_errors').hide();


    }

    this.emptyForm = function () {
        $fileFormName[0].reset();
        $fileFormName.find('input[name="id"]').val('');


    }
    this.init = function () {

        $self.bindFunction();
        $self.getStoreFiles();
        $self.initializeTable();


    }();
}