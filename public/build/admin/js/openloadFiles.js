var openloadFiles_moduel=new OpenloadFiles();
function OpenloadFiles(){
	var $self=this;
	 var csrfToken = $('meta[name="csrf-token"]').attr('content');
	 var $tableName=$('#filesTable');
	  var $formModal=$('#fileFormModal');
	  var $formName=$('#fileForm');
	
	 var dataTable="";
	

	this.bindFunction=function(){
		// disable/enable  Button
		$self.disableButton('#selectCountryPage #selectCountryBtn','yes');
		$self.disableButton('#filePage #importFilesBtn','yes');

		$('#selectCountryPage').on('change','select[name="country"]',function(){
			$self.disableButton('#selectCountryPage #selectCountryBtn','no');
		})
		// add/refresh Countries
		$('#selectCountryPage').on('click','#selectCountryBtn',function(){

			var id=$('#selectCountryPage').find('select[name="country"]').val();
			var countryId=$('#selectCountryPage').find('select[name="country"] option[value="'+id+'"]').attr('data-countryId');
			
			if(id==0){
				alert('Please Select Country!');
			}else{
				$self.getFiles(id);
				$('#filePage').find('#importFilesBtn').attr('data-countryId',countryId);
				$self.disableButton('#filePage #importFilesBtn','no');
			}
		})
		// import movies from selected countries

		$('#filePage').on('click','#importFilesBtn',function(){
			var id=$('#selectCountryPage').find('select[name="country"]').val();
			let countryFolderId=$(this).data('countryid');
			// console.log(countryFolderId);
			if(countryFolderId){
				$.get(openload.url+'file/listfolder?login='+openload.login+'&key='+openload.key+'&folder='+countryFolderId+'',function(response){
						// console.log(response);
					if(response.msg=='OK'){
						var formData ='&_token=' + csrfToken +'&countryId='+id;
						$.each(response.result.files,function(i,v){
							formData +='&countryFolderId[]='+v.folderid +'&name[]='+v.name +'&link[]='+v.link +'&linkTextId[]='+v.linkextid+'&size[]='+v.size+'&sha1[]='+v.sha1+'&downloadCount[]='+v.download_count;
						})

						$.post(base_url+'/control_panel/openload/fileMgmt/storeFiles',formData,function(response){
							$self.getFiles(id);
							console.log('success get file list from selected country folder and store to table');
						})
						
						
					}
					
				
				
					})

			}else{
				alert('Please Select Country!');
				return false;
			}
					
			});
// edit file
		
		$tableName.on('click','.editFileBtn',function(){
			$self.emptyForm();
				var id=$(this).data('id');
				
				$self.editFile(id);
				$formModal.modal('show');
		})

	// update 
		$formName.on('submit',function(e){
			e.preventDefault();
			var id=$formName.find('input[name="id"]').val();
			var countryId=$formName.find('input[name="countryId"]').val();
			var fileId=$formName.find('input[name="linkTextId"]').val();
			var fileName=$formName.find('input[name="name"]').val();
			$self.renameFileInOpenload(fileId,fileName,function(){
					$self.updateFile(countryId,id);
			})
		

			
			

		})
		
	}
	this.disableButton=function(selector,status){
		if(status=='yes'){
			$(selector).addClass('disabled');
		}else{
			$(selector).removeClass('disabled');
		}
		
	}

	this.initializeTable=function(){
		dataTable=$tableName.DataTable({
            processing: true,
        });
	}
	this.getFiles=function(countryId){

		$.get(base_url+'/control_panel/openload/fileMgmt/getFiles/'+countryId,function(response){
			var fileHtml='';
			$.each(response,function(i,v){
				fileHtml +='<tr>'+
								'<td>'+(i+1)+'</td>'+
								'<td>'+v.name+'</td>'+
								'<td>'+v.tmdbId+'</td>'+
								
								'<td>'+v.status+'</td>'+
								'<td>'+
								
								'<a class="btn btn-info editFileBtn " href="#" data-id="'+v.id+'" data-countryId="'+v.countryId+'"><i class="  fa fa-edit"></i></a>'+
								'</td>'+
							'</tr>';
			})
			if(dataTable==""){
				$self.initializeTable();
			}
			dataTable.destroy();
			$tableName.find('tbody').html(fileHtml);
			$self.initializeTable();
		})

	}
	this.editFile=function(id){

		$.get(base_url+'/control_panel/openload/fileMgmt/editFile/'+id,function(response){
			$formName.find('input[name="id"]').val(response.id);
			$formName.find('input[name="countryId"]').val(response.countryId);
			$formName.find('input[name="linkTextId"]').val(response.linkTextId);
			$formName.find('input[name="tmdbId"]').val(response.tmdbId);
			$formName.find('input[name="name"]').val(response.name);
			$formName.find('input[name="link"]').val(response.server.link);
			$formName.find('input[name="length"]').val(response.length);
			$formName.find('select[name="quality"]').val(response.quality);
			$formName.find('input[name="metaTitle"]').val(response.metaTitle);
			$formName.find('textarea[name="metaDescription"]').val(response.metaDescription);
			$formName.find('input[name="status"]').prop(response.status);
		})
	}
	this.renameFileInOpenload=function(fileId,fileName,callback){
		
		$.get(openload.url+'file/rename?login='+openload.login+'&key='+openload.key+'&file='+fileId+'&name='+fileName+'',function(response){
				console.log('success rename file ');
				if(response.msg=='OK'){
					callback();
				}else{
					return false;
				}
				
		})
		
	}

	this.updateFile=function(countryId,id){
		let formData =$formName.serialize()+'&_token=' + csrfToken;
		$.post(base_url+'/control_panel/openload/fileMgmt/updateFile/'+id,formData,function(response){
			$self.getFiles(countryId);
			$formModal.modal('hide');
			console.log('success update country');
		})
	}
	 this.hideError = function () {
        $formName.find('.form_errors').hide();


    }
    this.emptyForm = function () {
        $formName[0].reset();
        $formName.find('input[name="id"]').val('');

        $formName.find('select[name="serverType"]').val(0);
       

    }
	



	
	this.init = function () {
		
		$self.bindFunction();
	
       


    }();
}