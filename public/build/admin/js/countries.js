

var countries_module=new Countries();
function Countries(){
	var $self=this;
	 var csrfToken = $('meta[name="csrf-token"]').attr('content');
	 var $tableName=$('#countriesTable');
	 var $formName=$('#countryForm');
	 var $formModal=$('#countryFormModal');
	 var dataTable;
	this.folder={
		id:'',
		name:''
	};

	this.bindFunction=function(){
		// add/refresh Countries
		$('#addCountriesBtn').on('click',function(){
				$self.getCountryFolder()
		})
		// edit
		$tableName.on('click','.editCountryBtn',function(){
				var id=$(this).data('id');
				
				$self.editCountry(id);
				$formModal.modal('show');
		})
		// update 
		$formName.on('submit',function(e){
			e.preventDefault();
			var id=$formName.find('input[name="id"]').val();
			var countryId=$formName.find('input[name="countryId"]').val();
			var countryName=$formName.find('input[name="countryName"]').val();

			
			$self.renameOpenloadCountryFolder(countryId,countryName,function(){
				$self.updateCountry(id);
			})

			
			

		})
		
	}
	this.initializeTable=function(){
		dataTable=$tableName.DataTable({
            processing: true,
        });
	}

	this.getStoreCountries=function(){
		$.get(base_url+'/control_panel/openload/countryMgmt/getCountries',function(response){
			var countryHtml='';
			$.each(response,function(i,v){
				countryHtml +='<tr>'+
								'<td>'+(i+1)+'</td>'+
								'<td>'+v.countryName+'</td>'+
								'<td>'+v.status+'</td>'+
								'<td>'+
								
								'<a class="btn btn-info editCountryBtn " href="#" data-id="'+v.id+'" data-countryId="'+v.countryId+'"><i class="  fa fa-edit"></i></a>'+
								'</td>'+
							'</tr>';
			})
			dataTable.destroy();
			$tableName.find('tbody').html(countryHtml);
			$self.initializeTable();
		})

	}
	this.selectOpenloadFolder=function(){
		
		$.get(openload.url+'file/listfolder?login='+openload.login+'&key='+openload.key+'',function(response){
				// console.log(response);
				if(response.msg=='OK'){
					$.each(response.result.folders,function(i,v){
						if(v.name=='nmovies'){
							$self.folder.id=v.id;
							$self.folder.name=v.name;
						
						
							
						}
				})
					
				}
				
		})
		
	}
	this.getCountryFolder=function(folderId){
		
		
		$.get(openload.url+'file/listfolder?login='+openload.login+'&key='+openload.key+'&folder='+$self.folder.id+'',function(response){
				// console.log(response);
				if(response.msg=='OK'){
					$self.storeCountry(response.result.folders);
					console.log('success get countries list');
				}
					
				
				
		})
	}
	this.storeCountry=function(countriesFolder){
		let formData ='&_token=' + csrfToken;
		$.each(countriesFolder,function(i,v){
			formData +='&id[]='+v.id +'&name[]='+v.name;
		})

		$.post(base_url+'/control_panel/openload/countryMgmt/storeCountries',formData,function(response){
			$self.getStoreCountries();
			console.log('success store countries list');
		})
		

	}
	this.editCountry=function(id){

		$.get(base_url+'/control_panel/openload/countryMgmt/editCountry/'+id,function(response){
			$formName.find('input[name="id"]').val(response.id);
			$formName.find('input[name="countryId"]').val(response.countryId);
			$formName.find('input[name="countryName"]').val(response.countryName);
			$formName.find('input[name="status"]').prop(response.status);
		})
	}
	this.renameOpenloadCountryFolder=function(countryId,countryName,callback){
		
		$.get(openload.url+'file/renamefolder?login='+openload.login+'&key='+openload.key+'&folder='+countryId+'&name='+countryName+'',function(response){
				console.log('success rename countr folder');
				if(response.msg=='OK'){
					callback();
				}else{
					return false;
				}
				
		})
		
	}
	this.updateCountry=function(id){
			let formData =$formName.serialize()+'&_token=' + csrfToken;
		$.post(base_url+'/control_panel/openload/countryMgmt/updateCountry/'+id,formData,function(response){
			$self.getStoreCountries();
			$formModal.modal('hide');
			console.log('success update country');
		})
	}
	this.init = function () {
		 $self.selectOpenloadFolder();
		$self.bindFunction();
		$self.initializeTable();
		$self.getStoreCountries();
       


    }();
}