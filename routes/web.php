<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth Routing
Route::group([
    'namespace' => 'Admin'
], function () {
    Auth::routes();
});


//Admin Routing
Route::group([
    'namespace' => 'Admin',
    'prefix' => '/control_panel',
    'middleware' => ['auth']
], function () {
    Route::get('/', 'HomeController@index')->name('control_panel');
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/update/profile', 'ProfileController@update')->name('update.profile');

    // Route::get('/fileManager', 'FileManagerController@index')->name('fileManager.index');
    Route::resource('/menu', 'MenuController');
    Route::post('/menu/saveMenu/', 'MenuController@saveMenu')->name('menu.saveMenu');
    Route::get('/menu/getUrl/{id}', 'MenuController@getUrl')->name('menu.getUrl');


    Route::resource('/setting', 'SettingController');
    Route::get('getSetting', 'SettingController@getSetting')->name('setting.getSetting');
    // file management of movies and series
    Route::group(['prefix' => 'fileMgmt'], function () {
        Route::get('/', 'FileManagementController@index');
        Route::get('/getFiles', 'FileManagementController@getFiles');
        Route::post('/store', 'FileManagementController@store');
        Route::get('/edit/{id}', 'FileManagementController@edit');
        Route::post('/update/{id}', 'FileManagementController@update');
        Route::get('/delete/{id}', 'FileManagementController@delete');

        Route::group(['prefix' => 'server'], function () {
            Route::get('/', 'ServerController@index');
            Route::get('/getServers/{fileId}', 'ServerController@getServers');
            Route::post('/store', 'ServerController@store');
            Route::get('/edit/{id}', 'ServerController@edit');
            Route::post('/update/{id}', 'ServerController@update');
            Route::get('/delete/{id}', 'ServerController@delete');

        });

    });


    // openload Manager
    Route::group([
        'prefix' => 'openload'], function () {
        Route::group(['prefix' => 'countryMgmt'], function () {
            Route::get('/', 'OpenloadController@index');
            Route::get('/getCountries', 'OpenloadController@getCountries');
            Route::post('/storeCountries', 'OpenloadController@storeCountries');
            Route::get('/editCountry/{id}', 'OpenloadController@editCountry');
            Route::post('/updateCountry/{id}', 'OpenloadController@updateCountry');
            Route::get('/changeStatus/{id}/{status}', 'OpenloadController@changeStatus');
        });

        Route::group(['prefix' => 'fileMgmt'], function () {
            Route::get('/listFiles', 'OpenloadController@listFiles');
            Route::get('/getFiles/{countryId}', 'OpenloadController@getFiles');
            Route::post('/storeFiles', 'OpenloadController@storeFiles');
            Route::get('/editFile/{id}', 'OpenloadController@editFile');
            Route::post('/updateFile/{id}', 'OpenloadController@updateFile');

        });


    });


});


//Web routing
Route::group([
    'namespace' => 'Web'

], function () {

    Route::get('/', 'WebController@index')->name('web.index');
    Route::get('/movies', 'WebController@listMovies')->name('web.listMovies');
    Route::get('/series', 'WebController@listSeries')->name('web.listSeries');
    Route::get('/movie/{title}/', 'WebController@detailMovie')->name('web.detailMovie');
    Route::get('/tv/{title}/', 'WebController@detailTv')->name('web.detailTv');
    Route::get('/genre/{name}', 'WebController@listGenresVideos')->name('web.listGenresVideos');
    Route::get('/country/{name}', 'WebController@listCountryVideos')->name('web.listCountryVideos');
    Route::get('/{mediaType}/{type}/{name}', 'WebController@listMediaByType')->name('web.listMediaByType');
    Route::get('/list', 'WebController@listVideo')->name('web.listVideo');
    Route::get('/search', 'WebController@search')->name('web.search');

});
