<?php

namespace App\Libraries;
/**
 * API Doc=https://developers.themoviedb.org/3/
 */

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;


class TMDB extends Client
{
    public $url;
    public $urlWithKey;
    public $key;
    public $language = 'en-US';
    public $page = 1;

    function __construct($url, $key)
    {
        parent::__construct();
        $this->url = $url;
        $this->key = $key;

    }

    public function getAllMediaTypeVideos($mediaType, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);
        }
        return $this->getHttp($URI);

    }

//    $mediaType= 'movies','tv'
//$browseType='popular,rating,upcoming'
//optParam in Array=$optParam=[['field'=>'language','value'=>'en-US'],['field'=>'page','value'=>1]]
    /**
     * Get result lists of Movies or Tv series of browse type
     * @param $mediaType ,movie or tv
     * @param $browseType ,top rated, up coming, popular, now playing
     * @param array|null $optParam , language, page
     * @return mixed
     */
    public function getListVideos($mediaType, $browseType, array $optParam = null)
    {
        $URI = $this->url . $mediaType . '/' . $browseType . '?api_key=' . $this->key;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);
        }
        return $this->getHttp($URI);


    }

    /**
     * Get Video details of Movie or TV
     * @param $mediaType ,movie or tv
     * @param $mediaTypeId ,top rated, up coming, popular, now playing
     * @param array|null $optParam , language, page
     * @return mixed
     */
    public function getVideoDetails($mediaType, $mediaTypeId, array $optParam = null)
    {
        $URI = $this->url . $mediaType . '/' . $mediaTypeId . '?api_key=' . $this->key;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);
        }
        return $this->getHttp($URI);
    }

    /**
     * Get results of videos as type movie or tv
     * @param $mediaType ,movie or tv
     * @param $mediaTypeId ,media type id
     * @param array|null $optParam ,language, page,order
     * @return mixed
     */
    public function getVideos($mediaType, $mediaTypeId, array $optParam = null)
    {
        $URI = $this->url . $mediaType . '/' . $mediaTypeId . '?api_key=' . $this->key;
        if ($optParam) {
            foreach ($optParam as $param) {
                $URI .= $this->setOptionParam($optParam);
            }

        }
        return $this->getHttp($URI);
    }

    /**
     * Get results similar videos of media type
     * @param $mediaType ,movie or tv
     * @param $mediaTypeId ,media type id
     * @param array|null $optParam ,language, page,order
     * @return mixed
     */
    public function getSimilarVideos($mediaType, $mediaTypeId, array $optParam = null)
    {
        $URI = $this->url . $mediaType . '/' . $mediaTypeId . '/similar?api_key=' . $this->key;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }
        return $this->getHttp($URI);
    }

    /**
     * @param $mediaType
     * @param $type 'cast,company,crew,network'
     * @param $typeId
     * @param array|null $optParam
     * @return mixed
     */
    public function getMediaByType($mediaType, $type, $typeId, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key;
        switch ($type) {
            case 'cast':
                $URI .= '&with_cast='.$typeId;
                break;
            case 'crew':
                $URI .= '&with_people='.$typeId;
                break;
            case 'network':

                $URI .= '&with_networks='.$typeId;
                break;

            default :
                $URI .= '&with_companies='.$typeId;
        }
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);
        }

        return $this->getHttp($URI);
    }


    /**
     * Get results genres videos of media type
     * @param $mediaType ,movie or tv
     * @param array|null $optParam ,language, page,order
     * @return mixed
     */
    public function getGenre($mediaType, array $optParam = null)
    {

        $URI = $this->url . 'genre/' . $mediaType . '/list?api_key=' . $this->key;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }

        return $this->getHttp($URI);
    }

    /**
     *  Get results genres videos of media type
     * @param $mediaType ,movie or tv
     * @param $language , language
     * @param array|null $optParam ,page,order
     * @return mixed
     */
    public function getVideosByCountry($mediaType, $language, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_original_language=' . $language;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }

        return $this->getHttp($URI);
    }

    /**
     * Get results of videos info  of company
     * @param $mediaType ,movie or tv
     * @param $id ,company id
     * @param array|null $optParam ,language, page,order
     * @return mixed
     */
    public function getVideosByCompany($mediaType, $id, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_companies=' . $id;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }

        return $this->getHttp($URI);
    }

    /**
     * Get detail row of company
     * @param $id
     * @return mixed
     */
    public function getDetailsCompany($id)
    {
        $URI = $this->url . 'company/' . $id . '?api_key=' . $this->key;
        return $this->getHttp($URI);
    }


    /**
     * Get results of videos info of  network
     * @param $mediaType ,movie or tv
     * @param $id , network id
     * @param array|null $optParam ,language, page,order
     * @return mixed
     */
    public function getVideosByNetwork($mediaType, $id, array $optParam = null)
    {

        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_networks=' . $id;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }

        return $this->getHttp($URI);
    }

    /**
     * Get Results of videos info Genres
     * @param $mediaType ,movie or tv
     * @param $genre , genre action,horror etc
     * @param array|null $optParam ,order
     * @return mixed
     */
    public function getVideosByGenre($mediaType, $genre, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_genres=' . $genre;

        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }


        return $this->getHttp($URI);
    }

    /**
     * Get results of videos info Cast
     * @param $mediaType ,movie or tv
     * @param $id
     * @param array|null $optParam
     * @return mixed
     */
    public function getVideosByCast($mediaType, $id, array $optParam = null)
    {


        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_cast=' . $id;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);


        }
        return $this->getHttp($URI);
    }


    /**
     * Get Results of videos info of  Crew
     * @param $mediaType
     * @param $id
     * @param array|null $optParam
     * @return mixed
     */
    public function getVideosByCrew($mediaType, $id, array $optParam = null)
    {
        $URI = $this->url . 'discover/' . $mediaType . '?api_key=' . $this->key . '&with_people=' . $id;
        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }
        return $this->getHttp($URI);
    }

    /**
     * Search movie,tv,people/company series
     * @param $searchType
     * @param $query
     * @param array|null $optParam
     * @return mixed
     */
    public function search($searchType, $query, array $optParam = null)
    {
        $URI = $this->url . 'search/' . $searchType . '?api_key=' . $this->key . '&query=' . $query;

        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }
        return $this->getHttp($URI);
    }

    public function multiSearch($query, array $optParam = null)
    {
        $URI = $this->url . 'search/multi?api_key=' . $this->key . '&query=' . $query;

        if ($optParam) {
            $URI .= $this->setOptionParam($optParam);

        }
        return $this->getHttp($URI);
    }


    /**
     * Get Request
     * @param $URI
     * @return mixed
     */
    public function getHttp($URI)
    {


        $promise = $this->getAsync($URI, ['verify' => false])->then(function (ResponseInterface $res) {

            return ['status' => true, 'data' => json_decode($res->getBody())];
        }, function (RequestException $e) {

            $error = json_decode($this->getJsonFormatFromString($e->getMessage()));

            return ['status' => false, 'error' => $error];


//            return ['success' => false, 'status' => $error->status_code, 'status_message' => $error->status_message];

        });
        $response = $promise->wait();

        return $response;
    }


    /**
     * Get json format form string
     * @param $text
     * @return mixed
     */
    public function getJsonFormatFromString($text)
    {
        $pattern = '/\{(?:[^{}]|(?R))*\}/';
        preg_match_all($pattern, $text, $matches);
        return $matches[0][0];

    }

    private function setOptionParam($optParam)
    {
        $URI = '';

        foreach ($optParam as $field => $value) {
            $URI .= '&' . $field . '=' . $value;
        }
        return $URI;
    }


}
