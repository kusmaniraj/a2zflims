<?php namespace App\Traits;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait Paginate
{
    public function manualPaginate( $data, $totalResult, $perPage = null,$currentPage=null)
    {
        if ($perPage == null) {
            $perPage = 20;
        }
        $col = new Collection($data);

        $currentPageSearchResults = $col->slice(0, $perPage)->all();

        $entries = new LengthAwarePaginator($currentPageSearchResults, $totalResult, $perPage,$currentPage);
        return $entries->setPath(request()->url());

    }


}


