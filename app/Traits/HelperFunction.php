<?php namespace App\Traits;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait HelperFunction
{
    public function calStarRate($rating_average)
    {
        $html = '<ul class="w3l-ratings">';
        $whole = floor($rating_average/2);      // 1

        $half = $rating_average/2 - $whole; // .25
        $empty=5-$whole;
        for ($i = 0; $i < $whole; $i++) {
            $html .='<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>';
        }
        if($half > 0){
            $html .='<li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>';
            $empty --;
        }

        for ($i = 0; $i < $empty; $i++) {
            $html .='<li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>';
        }

//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>' .
        $html .= '</ul>';
        return $html;
    }
    public function characterLimit($string,$length){
        return substr($string,0,$length).'...';
    }


}

?>
