<?php namespace App\Traits;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait DataArray
{
    public function genres()
    {
        return [
            [
                'id' => 28,
                'name' => 'action'
            ],
            [
                'id' => 12,

                'name' => 'adventure'
            ],

            [
                'id' => 16,

                'name' => 'animation'
            ],
            [
                'id' => 35,

                'name' => 'comedy'
            ],
            [
                'id' => 18,

                'name' => 'crime'
            ],
            [
                'id' => 99,

                'name' => 'documentary'
            ],
            [
                'id' => 18,

                'name' => 'drama'
            ],
            [
                'id' => 10751,

                'name' => 'family'
            ],
            [
                'id' => "",

                'name' => 'kids'
            ],
            [
                'id' => 14,

                'name' => 'fantasy'
            ],
            [
                'id' => 36,
                'name' => 'history'
            ],
            [
                'id' => 27,

                'name' => 'horror'
            ],
            [
                'id' => 9648,

                'name' => 'mystery'
            ],
            [
                'id' => 10749,

                'name' => 'romance'
            ],

            [
                'id' => 53,

                'name' => 'thriller'
            ],
            [
                'id' => 10752,

                'name' => 'war'
            ],
            [
                'id' => 37,
                'tv_id' => 37,
                'name' => 'western'
            ],
        ];
    }

    public function countries()
    {
        return [
            [
                "iso_3166_1" => "af",
                "name" => "Afghanistan",


            ],

            [
                "iso_3166_1" => "cn",

                "name"=>'China'

            ],

            [
                "iso_3166_1" => "fr",

                "name"=>'France'

            ],
            [
                "iso_3166_1" => "fi",

                "name"=>'Finland'

            ],
            [
                "iso_3166_1" => "gl",

                "name"=>'Greenland'

            ],
            [
                "iso_3166_1" => "gr",

                "name"=>'Greece'

            ],
            [
                "iso_3166_1" => "it",

                "name"=>'Italy'

            ],
            [
                "iso_3166_1" => "id",

                "name"=>'Indonesia'

            ],
            [
                "iso_3166_1" => "ie",

                "name"=>'Ireland'

            ],
            [
                "iso_3166_1" => "hi",

                "name"=>'India'

            ],
            [
                "iso_3166_1" => "jp",

                "name"=>'Japan'

            ],
            [
                "iso_3166_1" => "ko",

                "name"=>'Korean'

            ],
            [
                "iso_3166_1" => "mm",

                "name"=>'Myanmar'

            ],
            [
                "iso_3166_1" => "mx",

                "name"=>'Mexico'

            ],
            [
                "iso_3166_1" => "my",

                "name"=>'Malaysia'

            ],
            [
                "iso_3166_1" => "kp",

                "name"=>'North Korea'

            ],
            [
                "iso_3166_1" => "ne",

                "name"=>'Nepal'

            ],
            [
                "iso_3166_1" => "no",

                "name"=>'Norway'

            ],
            [
                "iso_3166_1" => "nl",

                "name"=>'Netherlands'

            ],
            [
                "iso_3166_1" => "nz",

                "name"=>'New Zealand'

            ],

            [
                "iso_3166_1" => "ph",

                "name"=>'Philippines'

            ],

            [
                "iso_3166_1" => "pk",

                "name"=>'Pakistan'

            ],
            [
                "iso_3166_1" => "ru",

                "name"=>'Russia'

            ],
            [
                "iso_3166_1" => "sz",

                "name"=>'Swaziland'

            ],
            [
                "iso_3166_1" => "kr",

                "name"=>'South Korea'

            ],

            [
                "iso_3166_1" => "lk",

                "name"=>'Sri Lanka'

            ],

            [
                "iso_3166_1" => "es",

                "name"=>'Spain'

            ],


            [
                "iso_3166_1" => "ch",
                "name" => "Switzerland",


            ],
            [
                "iso_3166_1" => "en",
                "name" => "USA",


            ],
            [
                "iso_3166_1" => "en",
                "name" => "UK",


            ],


        ];
    }

}

?>
