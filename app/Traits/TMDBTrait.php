<?php namespace App\Traits;


use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait TMDBTrait
{
    public function manualPaginate($data, $totalResult, $perPage = null, $currentPage = null)
    {
        if ($perPage == null) {
            $perPage = 20;
        }
        $col = new Collection($data);

        $currentPageSearchResults = $col->slice(1, $perPage)->all();

        $entries = new LengthAwarePaginator($currentPageSearchResults, $totalResult, $perPage, $currentPage);
        return $entries->setPath(request()->url());

    }

    public function calStarRate($rating_average)
    {
        $html = '<ul class="w3l-ratings">';
        $whole = floor($rating_average / 2);      // 1

        $half = $rating_average / 2 - $whole; // .25
        $empty = 5 - $whole;
        for ($i = 0; $i < $whole; $i++) {
            $html .= '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>';
        }
        if ($half > 0) {
            $html .= '<li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>';
            $empty--;
        }

        for ($i = 0; $i < $empty; $i++) {
            $html .= '<li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>';
        }

//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>' .
//            '<li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>' .
        $html .= '</ul>';
        return $html;
    }

    public function characterLimit($string, $length)
    {
        return substr($string, 0, $length) . '...';
    }

    /**
     * Get Server API Videos to detail
     * @param $type
     * @param $tmdbId
     * $imdbId     * @return string[]
     */
    public function getServerVideos($type, $tmdbId, $imdbId)
    {
        $videos = [];
        if ($type == 'tv') {
            $videos = $this->getMoviesAPI($tmdbId, $imdbId);
        } else if ($type == 'tv') {
            $videos = $this->getSeriesAPI($tmdbId, $imdbId);
        }


        return $videos;

    }
    /**
     * Mapped videos to detail
     * @param $requestVideos
     * @return array
     */

    /**
     * Get API Array
     * @param $tmdbID
     * @param $videoInfo
     * @return string[]
     */
    private function getMoviesAPI($tmdbID, $imdbID)
    {
        return [
            'https://database.gdriveplayer.me/player.php?imdb=' . $imdbID,
            'https://gomostream.com/movie/' . $imdbID,
            'https://videospider.in/getvideo?key=bt1hMKtU7U5GJoaj&video_id=' . $imdbID,
            'https://api.123movie.cc/imdb.php?imdb=' . $imdbID,
            'https://archive.org/embed/' . $imdbID . '&autoplay=1',
            'http://movie2konline.net/api/openload.php?id=' . $imdbID,
            'https://videospider.stream/personal?key=U6Yl9ED4AjIr4W9I&video_id=' . $tmdbID . '&tmdb=1',

        ];
    }


    /**
     * Get API Array
     * @param $tmdbID
     * @param $imdbID
     * @return string[]
     */
    private function getSeriesAPI($tmdbID, $imdbID)
    {
        return [
            'https://database.gdriveplayer.me/player.php?imdb=' . $imdbID,
            'https://gomostream.com/movie/' . $imdbID,
            'https://videospider.in/getvideo?key=bt1hMKtU7U5GJoaj&video_id=' . $imdbID,
            'https://api.123movie.cc/imdb.php?imdb=' . $imdbID,
            'https://archive.org/embed/' . $imdbID . '&autoplay=1',
            'http://movie2konline.net/api/openload.php?id=' . $imdbID,
            'https://videospider.stream/personal?key=U6Yl9ED4AjIr4W9I&video_id=' . $tmdbID . '&tmdb=1',

        ];
    }

    public function mappedYoutubeTrailerVideos($requestVideos)
    {

        $videos = [];
        if ($requestVideos['status'] == true && isset($requestVideos['data']->videos)) {
            foreach ($requestVideos['data']->videos->results as $key => $video) {

                $youtube_data = (object)[
                    'path' => config('tmdb.youtube_embed') . $video->key . '?rel=0',
                    'key' => $video->key,
                    'id' => $video->id,

                ];

                $videos[] = $youtube_data;

            }
        }
        return $videos;

    }

    /**
     * mapped result  data
     * @param $type
     * @param array $tmdbData
     * @return object
     */
    public function mappedResultData($type, $tmdbData)
    {
        $mappingData = (object)[];

        if ($tmdbData) {
            $mappingData = $tmdbData->results;

            foreach ($mappingData as $key => $data) {
                $mappingData{$key} = $this->mappedRowData($type, $data);

//                if ($type == 'tv') {
//                    $mappingData{$key}->title = $data->name;
//                    $mappingData{$key}->original_title = $data->original_name;
//                    if (isset($data->first_air_date)) {
//                        $mappingData{$key}->release_date = $data->first_air_date;
//                    }
//                }
//                if (config('tmdb')) {
//                    $mappingData{$key}->poster_path = config('tmdb.poster_path') . $data->poster_path;
//                    $mappingData{$key}->backdrop_path = config('tmdb.backdrop_path') . $data->backdrop_path;
//                }
//                $carbon = Carbon::now(); //Today
//                $dateSixMonthBefore = $carbon->subMonth(6)->format('Y-m-d'); // Last day 12 months ago
//                if (isset($mappingData{$key}->release_date)) {
//                    $mappingData{$key}->release_year = date('Y', strtotime($mappingData{$key}->release_date));
//                    $mappingData{$key}->release_type = $mappingData{$key}->release_date >= $dateSixMonthBefore ? 'New' : '';
//                } else {
//                    $mappingData{$key}->release_date = '';
//                    $mappingData{$key}->release_year = '';
//                    $mappingData{$key}->release_type = '';
//                }
//                $mappingData{$key}->rating_stars = $this->calStarRate($data->vote_average);
//                $mappingData{$key}->media_type = $type;


            }
        }


        return $mappingData;
    }

    /**
     * mapped row data
     * @param $type
     * @param $tmdbData
     * @return object
     */
    public function mappedRowData($type, $tmdbData)
    {
        $mappingData = (object)[];


        if ($tmdbData) {

            $mappingData = $tmdbData;//assign to mapped variable
            /***Initialize variable*/
            $mappingData->release_date = '';
            $mappingData->release_year = '';
            $mappingData->release_type = '';
            $mappingData->rating_stars=5;
            /***End Initialize variable*/
            $mappingData->media_type = $type;//assign media type object to mapping data variable
            /******Mapped on Tv on certain field*******/
            if ($type == 'tv') {
                $mappingData->title = $mappingData->name;
                $mappingData->original_title = $mappingData->original_name;
                $mappingData->release_date = $mappingData->first_air_date;
                $mappingData->runtime = $mappingData->episode_run_time[0];

            }
            /******End of Mapped on Tv on certain field*******/

            /****Image Path***/
            if (config('tmdb')) {
                $mappingData->poster_path = config('tmdb.poster_path') . $mappingData->poster_path;
                $mappingData->backdrop_path = config('tmdb.backdrop_path') . $mappingData->backdrop_path;
            }
            /****End of Image Path***/

            /**Release Date**/
            $carbon = Carbon::now(); //Today
            $dateSixMonthBefore = $carbon->subMonth(6)->format('Y-m-d'); // Last day 12 months ago
            if ($mappingData->release_date) {
                $mappingData->release_year = date('Y', strtotime($mappingData->release_date));
                $mappingData->release_type = $mappingData->release_date >= $dateSixMonthBefore ? 'New' : '';
            }
            /**End of Release Date**/

            /**Rating Star**/
            if (isset($mappingData->vote_average)) {
                $mappingData->rating_stars = $this->calStarRate($mappingData->vote_average);
            }

            /**End of Rating Star**/
        }
        return $mappingData;

    }


}

