<?php namespace App\Traits;

use App\Model\Admin\Setting;
use App\Visitor;



trait Shared
{
    public function getSetting()
    {
        return Setting::first();
    }

    public function storeVisitorInfo(){
        $ip=request()->ip();
        $location=\Location::get($ip);
        $countryName='localhost';
        if($location){
            $countryName=$location->countryCode;
        }


        $storeData=[
            'IP'=>$ip,
            'country'=>$countryName
        ];
        return Visitor::create($storeData);
    }
    public function getVisitors(){
        return Visitor::all();
    }



}

