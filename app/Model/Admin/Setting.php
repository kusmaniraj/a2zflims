<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

protected $table = "setting";
 protected $fillable = ['created_at','_token','id', 'website_name', 'website_name_short','website_notice', 'fb_url','google_plus_url','skype_url','twitter_url','fb_page', 'logo', 'slogan', 'short_description', 'google_maps_iframe', 'owner_name', 'owner_email', 'owner_contact', 'owner_address', 'version'];
}