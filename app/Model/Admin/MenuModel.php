<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
   protected $table='menus';
    protected $fillable=['id','name','type','parentId','position','isDeletable','status','url'];
}
