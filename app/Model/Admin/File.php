<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table='files';
    protected $fillable=['id','tmdbId','videoUrl','status','downloadUrl','countryId','name','downloadCount','linkTextId','sha1','size','countryFolderId','quality','length','metaTitle','metaDescription'];
    public function country()
    {
    	return $this->belongsTo('App\Model\Admin\Country','countryId','id');
    }
    public function servers()
    {
        return $this->hasMany('App\Model\Admin\Server','fileId','id');
    }
    public function server()
    {
        return $this->hasOne('App\Model\Admin\Server','fileId','id');
    }

}
