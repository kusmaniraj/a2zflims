<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table='countries';
    protected $fillable=['id','countryId','countryName','status'];
}
