<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;


class Categories extends Model
{
    protected $table='categories';
    protected $fillable=['_token','id','name','parentId','position','isDeletable','status','created_at','featuredImg'];

    public function contents()
    {
        return $this->hasMany('App\Model\Admin\ContentsModel','categoryId');
    }
}
