<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $table='servers';
    protected $fillable=['id','name','link','fileId'];
    public function file()
    {
        return $this->belongsTo('App\Model\Admin\File','fileId','id');
    }
}
