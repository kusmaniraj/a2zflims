<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuildTree extends Controller
{
   

   public function buildTree(array $elements, $parentId = 0)
		{
		    $branch = array();
		    foreach ($elements as $element) {
		        if ($element['parent_id'] == $parentId) {
		            $children = $this->buildTree($elements, $element['id']);
		            if ($children) {
		                $element['children'] = $children;
		            }
		            $branch[] = $element;
		        }
		    }
		    return $branch;
		}

		function printNav($menu, $sub = false)
			{
			    foreach ($menu as $nav) {
			        $link_attrs = "";
			        $caret = "";
			        $drop_menu = "";

			        if (isset($nav['children'])) {
			            // $link_attrs = 'class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';
			            // $caret = '<span class="caret"></span>';
			            $drop_menu = '<ul>' . $this->printNav($nav['children'], true) . '</ul>';
			        }

			        echo '<li>';
			        echo '<a href="' . $nav['url'] . '" ' . $link_attrs . ' >' .'<i class="icon-circle"></i>'. $nav['text'] . ' ' . $caret . '</a>';
			        echo $drop_menu;
			        echo '</li>';
			    }

			}
			function buildMenu(array $menu_array, $is_sub = FALSE)
				{
				     $ul_attrs = $is_sub ? 'class="dropdown-menu"' :'class="nav navbar-nav"';
				    $menu = "<ul  $ul_attrs >";

				    foreach ($menu_array as $id => $attrs) {
				        $sub = isset($attrs['children'])
				            ? $this->buildMenu($attrs['children'], TRUE)
				            : null;
				        $li_attrs = $sub ? 'class="dropdown mega-dropdown"' : null;
				        // $a_attrs = $sub ? 'class="dropdown-toggle" data-toggle="dropdown"' : null;
				          $carat = $sub ? '<span class="fa fa-chevron-down pull-right"></span>' : null;
				         // $icon=$sub ? null:'<i class="icon-circle"></i>';
				        $menu .= "<li  $li_attrs>";
				        if($sub){
				        				$menu .= "<a href='" . $attrs["url"] . " '  class='dropdown-toggle' data-toggle='dropdown' role='button'
							   aria-haspopup='true' aria-expanded='false'>${attrs['text']}$carat</a>$sub";
							}else{
									$menu .= "<a href='" . $attrs["url"] . " '>${attrs['text']}</a>$sub";
							}

				        
				        $menu .= "</li>";
				    }

				    return $menu . "</ul>";
				}

				function buildFooterMenu(array $menu_array, $is_sub = FALSE)
				{
				     $ul_attrs = $is_sub ? null :'id="footer-menu-links"';
				    $menu = "<ul  $ul_attrs >";

				    foreach ($menu_array as $id => $attrs) {

				        $menu .= "<li>";
				        $menu .= "<a href='" . $attrs["url"] . "' >${attrs['text']}</a>";
				        
				        $menu .= "</li>";
				    }

				    return $menu . "</ul>";
				}
}
