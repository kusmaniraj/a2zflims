<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\API\TMDB\MovieController;
use App\Libraries\TMDB;
use App\Traits\DataArray;
use App\Traits\HelperFunction;
use App\Traits\Paginate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Session;


class WebController extends Controller
{
    use Paginate;
    use HelperFunction;
    use DataArray;

    protected $data;
    protected $tmdb;


    use Shared;

    public function __construct()
    {

        if (config('tmdb.base_uri') && config('tmdb.api_key')) {
            $this->tmdb = new TMDB(config('tmdb.base_uri'), config('tmdb.api_key'));

        } else {
            abort(500);
        }

        $this->data['setting'] = $this->getSetting();
        $this->data['genres'] = $this->genres();
        $this->data['countries'] = $this->countries();


    }

    public function index()
    {


        $this->storeVisitorInfo();//store visitor info


        $this->data['title'] = 'home';
        $this->data['now_playing'] = $this->getBrowserVideos('movie', 'now_playing');
        $this->data['popular'] = $this->getBrowserVideos('movie', 'popular');
        $this->data['top_rated'] = $this->getBrowserVideos('movie', 'top_rated');
//        $this->data['upcoming'] = $this->getBrowserVideos('movie', 'upcoming');
        $this->data['popularTvSeries'] = $this->getBrowserVideos('tv', 'popular');
        $this->data['top_ratedTvSeries'] = $this->getBrowserVideos('tv', 'top_rated');
        $this->data['upcomingTvSeries'] = $this->getBrowserVideos('tv', 'on_the_air');
        return view('web.index', $this->data);

    }


    /**
     * get list movies with pagination
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listMovies()
    {
        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $mappedData = [];
        $this->data['title'] = 'movies';
        $this->data['results_name'] = 'Movies';
        $media_type = 'movie';
        $optParam = [
            'language' => 'en-US',
            'page' => $page,
            'sort_by' => 'popularity.desc',

        ];


        $response = $this->tmdb->getAllMediaTypeVideos($media_type, $optParam);


        if ($response['status'] == true) {
            $mappedData = $this->mappedArrayData($media_type, $response);
        }
        $paginateResults = $this->manualPaginate($mappedData, $response['data']->total_results, 20, $page);
        $this->data['lists'] = $paginateResults;
        return view('web.list', $this->data);
    }

    /**
     * List of All tv Series with pagination
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listSeries()
    {
        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $mappedData = [];
        $this->data['title'] = 'tv-series';
        $this->data['results_name'] = 'Tv - Series';
        $media_type = 'tv';
        $optParam = [
            'language' => 'en-US',
            'page' => $page,
            'sort_by' => 'popularity.desc',

        ];

        $response = $this->tmdb->getAllMediaTypeVideos($media_type, $optParam);

        if ($response['status'] == true) {
            $mappedData = $this->mappedArrayData($media_type, $response);

        }
        $paginateResults = $this->manualPaginate($mappedData, $response['data']->total_results, 20, $page);

        $this->data['lists'] = $paginateResults;
        return view('web.list', $this->data);
    }


    /**
     * Get all browser lists (Up Coming, Latest,Top, Popular)
     * @param $type
     * @param $browserType
     * @return array|\Illuminate\Pagination\LengthAwarePaginator
     */
    private function getBrowserVideos($type, $browserType)
    {
        $paginateResults = [];
        $optParam = [
            'language' => 'en-US',
            'page' => 1,
            'region' => 'US',
            'sort_by' => 'popularity.desc',

        ];

        $response = $this->tmdb->getListVideos($type, $browserType, $optParam);


        if ($response['status'] == true) {
            $mappedData = $this->mappedArrayData($type, $response);
            $paginateResults = $this->manualPaginate($mappedData, $response['data']->total_results, 12);
        }

        return $paginateResults;
    }


    /**
     * Get Detail of Movie
     * @param $title
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function detailMovie($title)
    {

        $type = 'movie';
        $title = $this->decryptUrlTitle($title);
        $tmdbId = $this->getIDByTitle($title);
        $detailsMovie = $this->getDetail($type, $tmdbId);


        if (isset($detailsMovie) == false || empty($detailsMovie->info)) {
            abort(404);
        }

        $similarMovies = $this->similarMediaTypeVideos($type, $tmdbId);
        $this->data['details'] = $detailsMovie;
        $this->data['similarVideos'] = $similarMovies;
        $this->data['title'] = $title;
        return view('web.detail', $this->data);


    }

    /**
     * Get detail of TV Series
     * @param $title
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function detailTv($title)
    {


        $type = 'tv';
        $title = $this->decryptUrlTitle($title);
        $season = null;
        $episode = null;
        if (request()->has('season')) {
            $season = request()->get('season');
        }
        if (request()->has('episode')) {
            $episode = request()->get('episode');
        }

        $tmdbId = $this->getIDByTitle($title);
        $detailsTv = $this->getDetail($type, $tmdbId);

        if (isset($detailsTv) == false || empty($detailsTv->info)) {
            abort(404);
        }
        $similarTv = $this->similarMediaTypeVideos($type, $tmdbId);
        $this->data['details'] = $detailsTv;
        if ($season && $episode) {
            $videos = $this->getTvAPI($tmdbId, $season, $episode);
            if ($videos) {
                $this->data['showVideos'] = $videos;
            }

        }

        $this->data['similarVideos'] = $similarTv;
        $this->data['title'] = $title;
        return view('web.tv_series.detail', $this->data);
    }


    /**
     * Get Detail
     * @param $type
     * @param $tmdbId
     * @return object
     */
    public function getDetail($type, $tmdbId)
    {

        $detail = (object)[];

        $detail->defaultVideo = "";
        $detail->trailerVideo = "";
        $detail->media_type = $type;
        $detail->info = [];
        $detail->videos = [];
        $optParams = [
            'language' => 'en-US',
            'append_to_response' => 'credits,videos,external_ids',
        ];


        $detail_resp = $this->tmdb->getVideoDetails($type, $tmdbId, $optParams);
        if ($detail_resp['status'] == true) {

            $detail->info = $this->mappedData($type, $detail_resp['data']);
            $detail->trailerVideo = "";
            $detail->videos = $this->getServerVideos($type, $tmdbId, $detail_resp);

            if ($detail->videos) {
                $detail->defaultVideo = $detail->videos['gdriveplayer']; //gdriveplayer,gomostream,videospider-1,api.123movie,archive.org
                if ($trailer = $this->mappedYoutubeTrailerVideos($detail_resp)) {
                    $detail->trailerVideo = $trailer[0]->path;
                }

            }


        }


        return $detail;

    }


    /**
     * Get list of Genres with pagination
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listGenresVideos($name)
    {

        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }

        $listMovieVideos = [];
        $listTVVideos = [];

        foreach ($this->data['genres'] as $genre) {
            if ($genre['name'] == lcfirst($name)) {
                $optionalsParam = ['sort_by' => 'popularity.desc', 'page' => $page];
                $movieResult = $this->tmdb->getVideosByGenre('movie', $genre['id'], $optionalsParam);
                if ($movieResult['status'] == true) {
                    $movieMappedArray = $this->mappedArrayData('movie', $movieResult);
                    $listMovieVideos = $this->manualPaginate($movieMappedArray, $movieResult['data']->total_results, 20, $page);
                }


//                $tvs = $this->tmdb->getVideosByGenre('tv', $genre['id'], $optionalsParam);
//                $listTVVideos = $this->manualPaginate($tvs, $tvs['data']->total_results, 12, $page);
                break;
            }
        }

        $this->data['title'] = 'genres';
        $this->data['results_name'] = $name;
        $this->data['lists'] = $listMovieVideos;
//        $this->data['listTVVideos'] = $listTVVideos;

        return view('web.list', $this->data);
    }

    /**
     * Get list of countries with pagination
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listCountryVideos($name)
    {
        $name = str_replace('-', ' ', $name);

        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }

        $movies = [];
        $tvs = [];
        $optionalsParam = ['sort_by' => 'popularity.desc', 'page' => $page];
        foreach ($this->countries() as $country) {
            if ($country['name'] == $name) {
                $movieResult = $this->tmdb->getVideosByCountry('movie', $country['iso_3166_1'], $optionalsParam);
                if ($movieResult['status'] == true) {
                    $movieMappedArray = $this->mappedArrayData('movie', $movieResult);
                    $movies = $this->manualPaginate($movieMappedArray, $movieResult['data']->total_results, 20, $page);
                }
//                $tvs = $this->tmdb->getVideosByCountry('tv', $country['iso_639_1'], $optionalsParam);
//                if ($tvs['status'] == true) {
//                    $tvArray = $this->mappedArrayData('tv', $tvs);
//                }
                break;

            }

        }
        $this->data['title'] = 'countries';
        $this->data['lists'] = $movies;
        $this->data['results_name'] = ucFirst($name);
        return view('web.list', $this->data);
    }


    /**
     * @param $mediaType
     * @param $type 'company,crew,cast,network'
     * @param $typeName
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listMediaByType($mediaType, $type, $typeName)
    {
        $isValidParam = false;
        if ($mediaType == 'movie' || $mediaType == 'tv') {
            if ($type == 'cast' || $type == 'crew' || $type == 'company') {
                $isValidParam = true;
            }
            if ($mediaType == 'tv' && $type == 'network') {
                $isValidParam = true;
            }

        }

        if ($isValidParam === false) {
            abort(404);
        }


        $typeName = $this->decryptUrlTitle($typeName);
        $typeId = $this->getIDByTitle($typeName);
        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $lists = [];
        $optionalsParam = ['sort_by' => 'popularity.desc', 'page' => $page];
        $moviesResult = $this->tmdb->getMediaByType($mediaType, $type, $typeId, $optionalsParam);
        if ($moviesResult['status'] == true) {
            $movieMappedArray = $this->mappedArrayData('movie', $moviesResult);
            $lists = $this->manualPaginate($movieMappedArray, $moviesResult['data']->total_results, 20, $page);
        }


        $this->data['lists'] = $lists;
        $this->data['title'] = $typeName;
        $this->data['results_name'] = $typeName;
        return view('web.list', $this->data);
    }

    /**
     * Searching and Pagination of search input
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $this->data['title'] = 'Search';
        $page = 1;
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $listedSearch = [];
        if ($request->v == "") {
            return redirect()->back();
        }
        $optionalsParam = ['sort_by' => 'popularity.desc', 'page' => $page, 'region' => 'US'];
        $listResult = $this->tmdb->multiSearch($request->v, $optionalsParam);

        if ($listResult['status']) {
            $mappedResult = $this->mappedSearchArrayData($listResult);
            $listedSearch = $this->manualPaginate($mappedResult, $listResult['data']->total_results, 20, $page);
            $this->data['results_name'] = $request->v;

        }

        $this->data['lists'] = $listedSearch;
        $this->data['results_name'] = $request->v;
        return view('web.list', $this->data);

    }


    /**
     * Get Similar List
     * @param $media_type
     * @param $id
     * @return array|\Illuminate\Pagination\LengthAwarePaginator
     */
    private function similarMediaTypeVideos($media_type, $id)
    {
        $listVideos = [];
        $optionalsParam = ['sort_by' => 'popularity.desc'];
        $similar_videos = $this->tmdb->getSimilarVideos($media_type, $id, $optionalsParam);
        if ($similar_videos['status'] == true) {
            $mapped_similar_movies = $this->mappedArrayData($media_type, $similar_videos);
            $listVideos = $this->manualPaginate($mapped_similar_movies, $similar_videos['data']->total_results, 12);


        }
        return $listVideos;
    }

    /**
     * Get Server API Videos to detail
     * @param $type
     * @param $tmdbId
     * @param $detail_resp
     * @return string[]
     */
    private function getServerVideos($type, $tmdbId, $detail_resp)
    {

        $videos = $this->getMoviesAPI($tmdbId, $detail_resp);
        if ($this->mappedYoutubeTrailerVideos($detail_resp)) {
//            $videos['youtube'] = $this->mappedYoutubeTrailerVideos($detail_resp)[0]->path; //add youtube video to server
        } else {

            $videos['youtube'] = 'http://www.youtube.com/embed/INg83kArY4g?rel=0'; //add if api sever video not found then
        }
        return $videos;

    }


    /**
     * Mapped videos to detail
     * @param $requestVideos
     * @return array
     */
    private function mappedYoutubeTrailerVideos($requestVideos)
    {

        $videos = [];
        if ($requestVideos['status'] == true && isset($requestVideos['data']->videos)) {
            foreach ($requestVideos['data']->videos->results as $key => $video) {

                $youtube_data = (object)[
                    'path' => config('tmdb.youtube_embed') . $video->key . '?rel=0',
                    'key' => $video->key,
                    'id' => $video->id,

                ];

                $videos[] = $youtube_data;

            }
        }
        return $videos;

    }

    /**
     * mapped result  data
     * @param $type
     * @param array $tmdbData
     * @return object
     */
    private function mappedArrayData($type, array $tmdbData)
    {
        $mappingData = (object)[];

        if ($tmdbData && $tmdbData['status'] == true) {
            $mappingData = $tmdbData['data']->results;

            foreach ($mappingData as $key => $data) {

                $mappingData{$key} = $this->mappedData($type, $data);
            }


        }


        return $mappingData;
    }

    /**
     * @param array $tmdbData
     * @return object
     */
    private function mappedSearchArrayData(array $tmdbData)
    {
        $mappingData = (object)[];

        if ($tmdbData && $tmdbData['status'] == true) {
            $mappingData = $tmdbData['data']->results;

            foreach ($mappingData as $key => $data) {
                $type = $data->media_type;
                if ($type == 'tv' || $type == 'movie') {
                    $mappingData{$key} = $this->mappedData($type, $data);
                }

            }
        }


        return $mappingData;
    }

    /**
     * mapped row data
     * @param $type
     * @param $mappingData
     * @return object
     */
    private function mappedData($type, $mappingData)
    {

        /**Mapped Not exist in tmdb**/
        $mappingData->media_type = $type;
        $mappingData->runtime = null;
        $mappingData->release_year = null;
        $mappingData->release_type = null;
        /**Mapped Not exist in tmdb**/

        if(isset($mappingData->name)){
            $mappingData->title = $mappingData->name;
        }
        if(isset($mappingData->original_name)){
            $mappingData->original_title = $mappingData->original_name;
        }
        if(isset($mappingData->episode_run_time) &&  $mappingData->episode_run_time){
            $mappingData->runtime=$mappingData->episode_run_time[0];
        }
        if(isset($mappingData->original_name)){
            $mappingData->release_date = $mappingData->first_air_date;
        }


        /***Poster and Backdrop**/

        if (config('tmdb')) {
            $mappingData->poster_path = config('tmdb.poster_path') . $mappingData->poster_path;
            $mappingData->backdrop_path = config('tmdb.backdrop_path') . $mappingData->backdrop_path;
        }
        /*** /Poster and Backdrop**/

        /** Release Year and Release type **/

        $carbon = Carbon::now(); //Today
        $dateSixMonthBefore = $carbon->subMonth(6)->format('Y-m-d'); // Last day 12 months ago
        if (isset($mappingData->release_date)) {
            $mappingData->release_year = date('Y', strtotime($mappingData->release_date));
            $mappingData->release_type = $mappingData->release_date >= $dateSixMonthBefore ? 'New' : '';
        }
        /** /Release Year and Release type **/

        /** Rating Star **/
        $mappingData->rating_stars = $this->calStarRate($mappingData->vote_average);
        /** /Rating Star **/

        return $mappingData;

    }


    /**
     * Get API Array
     * @param $tmdbID
     * @param $videoInfo
     * @return string[]
     */
    private function getMoviesAPI($tmdbID, $videoInfo)
    {
        return [
            'gdriveplayer' => 'https://database.gdriveplayer.me/player.php?imdb=' . $videoInfo['data']->external_ids->imdb_id,

            'gomostream' => 'https://gomostream.com/movie/' . $videoInfo['data']->external_ids->imdb_id,
            'videospider-1' => 'https://videospider.in/getvideo?key=bt1hMKtU7U5GJoaj&video_id=' . $videoInfo['data']->external_ids->imdb_id,
            'api.123movie' => 'https://api.123movie.cc/imdb.php?imdb=' . $videoInfo['data']->external_ids->imdb_id,
            'archive.org' => 'https://archive.org/embed/' . $videoInfo['data']->external_ids->imdb_id . '&autoplay=1',
            'movie2konline' => 'http://movie2konline.net/api/openload.php?id=' . $videoInfo['data']->external_ids->imdb_id,
            'videospider' => 'https://videospider.stream/personal?key=U6Yl9ED4AjIr4W9I&video_id=' . $tmdbID . '&tmdb=1',
            'fsapi' => 'https://fsapi.xyz/movie/' . $videoInfo['data']->external_ids->imdb_id,

        ];
    }

    private function getTvAPI($tmdbID, $season, $episode)
    {
        return [
//            'fsapi'=>'https://fsapi.xyz/tv-imdb/'.$videoInfo['data']->external_ids->imdb_id,
            'https://fsapi.xyz/tv-tmdb/' . $tmdbID . '-' . $season . '-' . $episode

        ];
    }

    private function getIDByTitle($title)
    {
        $id = null;
        $movieList = $this->tmdb->multiSearch($title);

        if (isset($movieList['data']) && $movieList['data']->results) {

            $id = $movieList['data']->results[0]->id;
        }

        return $id;

    }

    private function encryptUrlTitle($string)
    {
        return str_replace(' ', '-', $string);
    }

    private function decryptUrlTitle($string)
    {
        return str_replace('-', ' ', $string);
    }


}
