<?php

namespace App\Http\Controllers\Admin;

use App\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    private $data;
    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
         $this->data['title']='Dashboard';


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['visitors']=Visitor::whereDate('created_at','=',date('Y-m-d'))->orderBy('created_at','desc')->get();
        return view('admin/dashboard',$this->data);
    }
}
