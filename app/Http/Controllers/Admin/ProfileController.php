<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use Session;
use Validator;
use Hash;
use File;
use Image;
use Input;
use App\Model\Admin\User;
use DB;

class ProfileController extends Controller
{
	private $data;

    public function __construct()
    {
        $this->data['getSetting']=SettingController::getSetting();
         $this->data['title']='Profile';



    }
    public function index(){
    	
    	
    	return view('admin/profile',$this->data);
    }
    public function update(Request $request){
    		$inputData=$request->all();
            $image_name=$inputData['old_profileImg'];
    		$validator=Validator::make($inputData, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',
            'gender'=>'required',
            
            
       	 ]);
    		if($validator->fails()){
    			return redirect()->back()->withErrors($validator);
    		}
            if (empty($inputData['profileImg'])) {
               $inputData['profileImg']=$inputData['old_profileImg'];
               unset($inputData['old_profileImg']);
            }else{
                 $inputData['profileImg']=$this->imgUpload($request->file('profileImg'));
                 if($image_name){
                     $pathImg=public_path('files/1/profile/'.$image_name);
                    if(file_exists($pathImg) ){
                            @unlink($pathImg);
                        }
                 }
            }

    		if(empty($inputData['current_password'])&&empty($inputData['password'])&&empty($inputData['password_confirmation'])){
    			$inputData['password']=Auth::user()->password;
                
    		}else{
    			
                if(empty($inputData['current_password'])){
                    Session::flash('incorrect', 'Required Current Password');
                    return redirect()->back();
                }
    			if(!Hash::check($inputData['current_password'],Auth::user()->password)){
    				Session::flash('incorrect', 'Incorrect Current Password');
    				return redirect()->back();
    			}
    			$validator=Validator::make($inputData, [
            			'password' => 'required|string|min:6|confirmed',
                        
          				 ]);
    			if($validator->fails()){
    				return redirect()->back()->withErrors($validator);
    				}

    			if($inputData['password']==$inputData['password_confirmation']){
    			 $inputData['password']=bcrypt($inputData['password']);
    			 unset($inputData['current_password']);
    			 unset($inputData['password_confirmation']);
    			
    			}
    		}
    		
    		
    		$user_data=User::find(Auth::user()->id)->update($inputData);
    		if($user_data){
                    
        			
                  Session::flash('success', 'Update Data uccessfully');
            
    			
    		}else{
    			 Session::flash('error', 'Update Data Unsuccessfully');
    		}
    		return redirect()->back();
    		


    }
    public function imgUpload($image){
        if($image){
             $path=public_path('files/1/profile/');
            if (!file_exists($path)) {
            $result = File::makeDirectory($path, 0775, true);
            }
                 $image_name= time().'.png';
               $path2 = public_path() . "/files/1/profile/" . $image_name;
                Image::make($image)->resize(200,200)->save($path2);
                return $image_name;
       

       }
       
    }

}
