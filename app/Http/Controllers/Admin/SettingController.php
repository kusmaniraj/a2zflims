<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Model\Admin\Setting;
use File;
use Image;

class SettingController extends Controller
{
    public function __construct()
    {
         $this->data['getSetting']=$this->getSetting();
         $this->data['title']='Setting';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['settings']=Setting::first();
        return view('admin/setting',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formInput=$request->all();
        $formInput['logo']=$this->imgUpload($request->file('logo'));

        unset($formInput['oldLogo']);
        if(Setting::insert($formInput)){
            Session::flash('success','ADD Data Successfully');



        }else{
            Session::flash('error',' Cannot Add Data Successfully');
        }
       return redirect()->back();



}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $formInput=$request->all();
        $image_name=$formInput['oldLogo'];
        if(empty($formInput['logo'])){

            $formInput['logo']=$formInput['oldLogo'];
        }else{
             $formInput['logo']=$this->imgUpload($request->file('logo'));
             if($image_name){
                $pathImg=public_path('files/1/logo/'.$image_name);
               if(file_exists($pathImg) ){
                     @unlink($pathImg);
                 }
             }
        }
        unset($formInput['oldLogo']);
        if(Setting::find($id)->update($formInput)){
            
            Session::flash('success','Update Data Successfully');



        }else{
            Session::flash('error',' Cannot Update Data Successfully');
        }
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public static function getSetting(){
        $result=Setting::first();
        return $result;

    }
    public function imgUpload($image){
         if($image){
        $path=public_path('files/1/logo/');
        if (!file_exists($path)) {
        $result = File::makeDirectory($path, 0775, true);
        }
                 $image_name= time().'.png';
               $path2 = public_path() . "/files/1/logo/" . $image_name;
                Image::make($image)->resize(200,200)->save($path2);
                return $image_name;
       

       }
    }
}
