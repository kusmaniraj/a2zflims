<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Server;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Validator;

class ServerController extends Controller
{
    use Shared;
    protected $data;

    public function __construct(){
        $this->data['getSetting']=$this->getSetting();

    }
    public function index()
    {
        $this->data['title']='Server Management';
       
        return view("admin/server/list", $this->data);

    }
    public function getServers($fileId){
        return Server::where(['fileId'=>$fileId])->with('file')->get();
    }
    public function store(Request $request){

        $formInput=$request->all();

        $validator=Validator::make($request->all(),
            ['name'=>'required',
                'link'=>'required',
                'fileId'=>'required',
              

            ]);
        if($validator->fails()){

            return ['status'=>'validation_errors','msg'=>$validator->errors()];
        }

        $result=Server::create($formInput);

        if($result){
            $store_msg=['status'=>'success','msg'=>'add servers'];
        }else{
            $store_msg=['status'=>'error','msg'=>'cannot add servers'];
        }
        return $store_msg;
    }
    public function edit($id){
        return Server::find($id);
    }

    public function update(Request $request,$id){

        $formInput=$request->all();
        $validator=Validator::make($request->all(),
            ['name'=>'required',
                'link'=>'required',
                'fileId'=>'required',


            ]);
        if($validator->fails()){

            return ['status'=>'validation_errors','msg'=>$validator->errors()];
        }
        $result=Server::find($id)->update($formInput);
        if($result){
            $store_msg=['status'=>'success','msg'=>'update selected server'];
        }else{
            $store_msg=['status'=>'error','msg'=>'cannot update selected server'];
        }
        return $store_msg;
    }
    public function delete($id){

        $result=Server::find($id)->delete();
        if($result){
            $store_msg=['status'=>'success','msg'=>'delete selected server'];
        }else{
            $store_msg=['status'=>'error','msg'=>'cannot delete selected server'];
        }
        return $store_msg;
    }
}
