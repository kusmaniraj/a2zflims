<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\File;

use App\Traits\Shared;
use Validator;
class FileManagementController extends Controller
{
    use Shared;
    protected $data;

   public function __construct(){
    	 $this->data['getSetting']=$this->getSetting();
    	
    }
     public function index()
    {
    	 $this->data['title']='File Management';

        return view("admin/file/list", $this->data);

    }
    public function getFiles(){
    	return File::with(['country','servers'])->get();
    }
    public function store(Request $request){

    	$formInput=$request->all();
    	
    	$validator=Validator::make($request->all(),
    		['name'=>'required',

                'countryId'=>'required',

    		
                'quality'=>'required',
                'length'=>'required',
    			'tmdbId'=>'required|numeric'

    			],['required.countryId'=>'Please select country']);
    	if($validator->fails()){
    		
    		return ['status'=>'validation_errors','msg'=>$validator->errors()]; 
    	}

    	$result=File::create($formInput);
		
		if($result){
				$store_msg=['status'=>'success','msg'=>'store files'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot store files'];
		}
		return $store_msg;
    }
    public function edit($id){
    	return File::find($id);
    }
    
    public function update(Request $request,$id){

    	$formInput=$request->all();
        $validator=Validator::make($request->all(),
            ['name'=>'required',

                'countryId'=>'required',
               
                'quality'=>'required',
                'length'=>'required',
                'tmdbId'=>'required|numeric'

                ],['required.countryId'=>'Please select country']);
        if($validator->fails()){
            
            return ['status'=>'validation_errors','msg'=>$validator->errors()]; 
        }
         $result=File::find($id)->update($formInput);
    	if($result){
				$store_msg=['status'=>'success','msg'=>'update selected file'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot update selected file'];
		}
		return $store_msg;
    }
     public function delete($id){
    
         $result=File::find($id)->delete();
    	if($result){
				$store_msg=['status'=>'success','msg'=>'delete selected file'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot delete selected file'];
		}
		return $store_msg;
    }
}
