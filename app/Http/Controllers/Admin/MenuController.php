<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\MenuModel;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\BuildTree;
use App\Model\Admin\ContentsModel;
use DB;
use Validator;
use Illuminate\Support\Facades\Session;
use \stdClass;
use Image;
use Input;
use File;
use App\Traits\Shared;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $data;
    use Shared;

    public function __construct()
    {

        $this->data['getSetting']=$this->getSetting();
         $this->data['title']='Menu';
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id','position')->orderBy('position')->get()->toArray();

        // $this->data['menus'] =BuildTree::buildTree($menuLists);
         $buildTree=new BuildTree;
        $this->data['menus'] = $buildTree->buildTree($menuLists);

    }

    public function index()
    {
        

        return view("admin/menu/list", $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $formInput = $request->all();
        $formInput['isDeletable']='yes';
         
        Validator::make($request->all(), [
            'name' => 'required|max:20',

        ],['name.required'=>'*Please Input Menu Name*'])->validate();


        if (MenuModel::insert($formInput)) {
            Session::flash('success', 'Add Data Successfully');
            return redirect('control_panel/menu');


        } else {
            echo "error";
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['menu_detail'] = MenuModel::find($id);
        return view("admin/menu/list", $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $formData = $request->all();
       
        $formData['isDeletable']='yes';
        Validator::make($request->all(), [
            'name' => 'required|max:20',

        ],['name.required'=>'*Please Input Menu Name*'])->validate();

        
        if (MenuModel::find($id)->update($formData)) {
            
            Session::flash('success', 'Update Menu Successfully');

        } else {
            Session::flash('error', 'Cannot Updated Menu Successfully');

        }
        return redirect()->route('menu.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             $image_name=MenuModel::select('featuredImg')->where('id',$id)->first();  

        if (MenuModel::find($id)->delete()) {
             $this->deleteImg($image_name['featuredImg']);
            Session::flash('success', 'Delete Menu Successfully');


        } else {
            Session::flash('error', 'Cannot Delete Menu Successfully');
        }


    }

    

    public function saveMenu(Request $request)
    {
        $menus = $request->input('treeExport');

        foreach ($menus as $key => $menu) {

            $menu['position'] = $key;
            MenuModel::where('id',$menu['id'])->update($menu);

        }

    }

    public function imgUpload($image){
         if($image){
                $image_name= time().'.png';
               

                 $path = public_path() . "/files/1/" . $image_name;
                Image::make($image)->resize(1366,281)->save($path);
                $path2 = public_path() . "/files/1/thumbs/" . $image_name;
                Image::make($image)->resize(200,200)->save($path2);
                return $image_name;


      
       

       }
}
    public function deleteImg($image_name){
        $pathImg1=public_path('files/1/'.$image_name);
        if(file_exists($pathImg1) ){
            @unlink($pathImg1);
        }
        $pathImg2=public_path('files/1/thumbs/'.$image_name);

        if(file_exists($pathImg2) ){
            @unlink($pathImg2);
        }
        return true;
    }

    public function getUrl($categoryId){
        $menuUrl=Categories::with(['contents'=>function($query){
            $query->where('status','active');
        }])->where(['status'=>'active','id'=>$categoryId])->get();
        return $menuUrl;
    }

}
