<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Server;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use App\Model\Admin\Country;
use App\Model\Admin\File;
class OpenloadController extends Controller
{
    private $data;
    use Shared;
    public function __construct(){
    	 $this->data['getSetting']=$this->getSetting();
    	
    }
     public function index()
    {
    	 $this->data['title']='Country Management';
        return view("admin/file/server/openload/listCountries", $this->data);

    }
    public function getCountries(){
    	return Country::all();
    }
    public function storeCountries(Request $request){

    	$formInput=$request->all();
    	$result=false;
    	// remove all Countries
    
		for($i=0;$i<count($formInput['id']);$i++){

    		
    		if(count(Country::where(['countryId'=>$formInput['id'][$i]])->first()) >0){
				Country::where(['countryId'=>$formInput['id'][$i]])->update([
	    			'countryName'=>$formInput['name'][$i],
	    			]);
					
			}else{
				Country::create(['countryId'=>$formInput['id'][$i],'countryName'=>$formInput['name'][$i]]);
			}
			
    		$result=true;
    	}
		
		if($result){
				$store_msg=['status'=>'success','msg'=>'store countries'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot store countries'];
		}
		return $store_msg;
    }
    public function editCountry($id){
    	return Country::find($id);
    }
    public function updateCountry(Request $request,$id){
    	$formInput=$request->all();
    	$result=Country::find($id)->update($formInput);
    	if($result){
				$store_msg=['status'=>'success','msg'=>'update country'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot update country'];
		}
		return $store_msg;
    }

    public function changeStatus($id,$status){
    	
    	$result=Country::find($id)->update(['status'=>$status]);
    	if($result){
				$store_msg=['status'=>'success','msg'=>'change status country'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot status country'];
		}
		return $store_msg;
    }
    public function listfiles(){
 				$this->data['title']='Files Management';
 				$this->data['countries']=Country::all();
        		return view("admin/file/server/openload/listFiles", $this->data);
    }
    public function getFiles($countryId){
    		return File::where(['countryId'=>$countryId])->with(['servers'=>function($query){
			$query->where('name','openload_user');
			}])->get();
    }
    public function storeFiles(Request $request){

    	$formInput=$request->all();
      
    	$result=false;
    	
    
		for($i=0;$i<count($formInput['name']);$i++){
			if(count(File::where(['linkTextId'=>$formInput['linkTextId'][$i]])->first()) >0){
				File::where(['linkTextId'=>$formInput['linkTextId'][$i]])->update([
	    			'name'=>$formInput['name'][$i],
	    			]);
					
			}else{

					$insertFile=File::create([
	    			'name'=>$formInput['name'][$i],
	    			'countryFolderId'=>$formInput['countryFolderId'][$i],
	    			'countryId'=>$formInput['countryId'],

	    			'linkTextId'=>$formInput['linkTextId'][$i],
	    			'size'=>$formInput['size'][$i],
	    			'sha1'=>$formInput['sha1'][$i],
	    			'downloadCount'=>$formInput['downloadCount'][$i],

	    			

	    		]);

				Server::create(['fileId'=>$insertFile->id,'name'=>'openload_user','link'=>$formInput['link'][$i]]);
			}
    		
    		
    		$result=true;
    	}
		
		if($result){
				$store_msg=['status'=>'success','msg'=>'store files'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot store files'];
		}
		return $store_msg;
    }
     public function editFile($id){
    	return File::with('server')->whereHas('server',function($query) use ($id){
			$query->where(['name'=>'openload_user','fileId'=>$id]);
		})->first();
    }
    public function updatefile(Request $request,$id){
    	$formInput=$request->all();
          $formInput['serverType']='openload';
    	$result=File::find($id)->update($formInput);
    	if($result){
				$store_msg=['status'=>'success','msg'=>'update selected file'];
		}else{
				$store_msg=['status'=>'error','msg'=>'cannot update selected file'];
		}
		return $store_msg;
    }

}
