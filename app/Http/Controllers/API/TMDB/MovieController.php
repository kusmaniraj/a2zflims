<?php


namespace App\Http\Controllers\API\TMDB;


use App\Http\Controllers\Controller;
use App\Libraries\TMDB;

use App\Traits\TMDBTrait;
use Illuminate\Http\Request;


class MovieController extends Controller
{
  use TMDBTrait;
    protected $data;
    private $tmdbConfig;
    private $tmdbLib;
    private $mediaType='movie';

    public function __construct()
    {
        $this->tmdbConfig=config('tmdb');



        //        $this->storeVisitorInfo();//store visitor info
        if ($this->tmdbConfig) {
            $this->tmdbLib = new TMDB(  $this->tmdbConfig['base_uri'],   $this->tmdbConfig['api_key']);
        } else {
          return ['status'=>false,'error'=>'Invalid TMDG Configuration.'];
        }
    }

    /**
     * Get All List Movies
     * @return mixed
     */
    public function getResults(){

        $page = 1;//page number
        $perPage=20;//per page row
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $optParam = [
            'language'=>'en-US',
            'page'=>$page,
            'sort_by'=>'popularity.desc',

        ];


        $response= $this->tmdbLib->getAllMediaTypeVideos($this->mediaType,$optParam);

        if ($response['status'] == true) {
            $totalResults=$response['data']->total_results;
            $mappedData = $this->mappedResultData($this->mediaType,$response['data']);
            $response['data']=$this->manualPaginate($mappedData, $totalResults, $perPage, $page);
        }
            return  $response;




    }

    /**
     * Get Movie Detail
     * @param $tmdbId
     * @return mixed
     */
    public function getRow($tmdbId){
        $detail = (object)[];
        $optParams = [
            'language'=>'en-US',
            'append_to_response'=>'credits,videos,external_ids',
        ];
        $response = $this->tmdbLib->getVideoDetails($this->mediaType, $tmdbId, $optParams);


        if ($response['status'] == true) {
            $detail->info = $this->mappedRowData($this->mediaType, $response['data']);
            $detail->trailerVideo = "";
            $detail->videos = $this->getServerVideos($this->mediaType, $tmdbId, $response);
            if ($detail->videos) {
                $detail->defaultVideo = $detail->videos['gdriveplayer']; //gdriveplayer,gomostream,videospider-1,api.123movie,archive.org
                $detail->trailerVideo = $this->mappedYoutubeTrailerVideos($response)[0]->path;
            }


        }

        return $response;



    }


    /**
     * Get all browser lists (now_playing,popular, top_rated,upcoming)
     * @param $browserType
     * @return mixed
     */
    public function getResultsByBrowserType($browserType)
    {
        $page=1;
        $perPage=12;

        $optParam = [
            'language'=>'en-US',
            'page'=>$page,
            'sort_by'=>'popularity.desc',

        ];
        $response = $this->tmdbLib->getListVideos($this->mediaType, $browserType, $optParam);
        if ($response['status'] == true) {
            $totalResults=$response['data']->total_results;
            $mappedData = $this->mappedResultData($this->mediaType, $response['data']);
            $response['data'] = $this->manualPaginate($mappedData, $totalResults, $perPage);
        }

        return $response;
    }

    /**
     * Get Results of Similar Movies from tmdb id
     * @param $tmdbid
     * @return mixed
     */
    public function getResultsBySimilar($tmdbid)
    {
        $listVideos = [];
        $perPage=12;
        $optionalsParam = ['sort_by' => 'popularity.desc'];
        $response = $this->tmdbLib->getSimilarVideos($this->mediaType, $tmdbid, $optionalsParam);
        if ($response['status'] == true) {
            $mapped_similar_movies = $this->mappedResultData($this->mediaType, $response['data']);
            $totalResults=$response['data']->total_results;
            $response['data'] = $this->manualPaginate($mapped_similar_movies,$totalResults , $perPage);


        }
        return $response;
    }


    /**
     * Search Movies
   **/
    public function search()
    {
        $this->data['title'] = 'Search';
        $page = 1;
        $perPage=20;
        if (request()->has('page')) {
            $page = request()->get('page');
        }
        $searchValue='';
        if (request()->has('v')) {
            $searchValue = request()->get('v');
        }
        $optionalsParam = ['sort_by' => 'popularity.desc',  'page'=> $page];
        $response = $this->tmdbLib->search($this->mediaType, $searchValue, $optionalsParam);

        if ($response['status']) {
            $mappedMovies = $this->mappedResultData('movie', $response['data']);
            $totalResults=$response['data']->total_results;
            $response['data'] = $this->manualPaginate($mappedMovies, $totalResults, $perPage, $page);


        }

        return $response;



    }

}
