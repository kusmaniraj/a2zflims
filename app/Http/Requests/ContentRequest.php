<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'categoryId'=>'required',
            'description'=>'required',

        ];
    }
    public function messages()
    {
        return [
            'name.required' => '*Please Input Content Name*',
            'categoryId.required' => '*Please Select Categories*',
            'description.required'=>'*Please Input Description*'


        ];
    }
}
