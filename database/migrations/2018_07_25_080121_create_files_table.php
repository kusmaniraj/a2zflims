<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->integer('tmdbId')->nullable();
            $table->integer('countryId')->unsigned()->nullable();
            $table->foreign('countryId')->references('id')->on('countries')->onDelete('cascade')->onDelete('cascade');
            $table->integer('countryFolderId')->nullable();;

            $table->string('linkTextId')->nullable();;
            $table->integer('size')->nullable();;
            $table->integer('length')->nullable();
            $table->enum('quality',['HD','SD','TS','CAM']);
            $table->string('sha1')->nullable();
            $table->integer('downloadCount')->nullable();
            $table->string('downloadUrl')->nullable();
            $table->string('videoUrl')->nullable();
            $table->string('metaTitle')->nullable();
            $table->string('metaDescription')->nullable();
            $table->enum('status',['active','inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
